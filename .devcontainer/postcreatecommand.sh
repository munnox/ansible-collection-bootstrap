#! /bin/bash
dnf install -y unzip

curl -L 'https://vault.bitwarden.com/download/?app=cli&platform=linux' --output /usr/local/bin/bw.zip \
    && unzip /usr/local/bin/bw.zip -d /usr/local/bin/ \
    && chmod +x /usr/local/bin/bw \
    && rm /usr/local/bin/bw.zip

pip install poetry
# /usr/local/bin/poetry config virtualenvs.create false
poetry install --with dev

PYPATH=$(poetry env info -p)
echo ${PYPATH}

id
# echo "PATH=\"$(poetry env info -p)/bin/:$PATH\"" >> /etc/environment
# echo "export PATH=\"${PYPATH}/bin/:$PATH\"" >> ~/.zshrc
echo "export PYPATH=\"${PYPATH}\"" >> /etc/zshrc
echo "export PATH=\"${PYPATH}/bin/:\$PATH\"" >> /etc/zshrc
echo ". /etc/zshrc" >> /root/.zshrc
# echo "PYPATH=\"${PYPATH}/bin/\"" >> /etc/environment
# echo "export PYPATH=\"${PYPATH}/bin/\"" >> ~/.zshrc
# echo "PATH=\"$PYPATH:$PATH\"" >> /etc/environment
# echo "PATH=\"${PYPATH}/bin/:$PATH\"" >> /root/.bashrc
# echo ". /etc/environment" >> /root/.bashrc
# echo "PATH=\"$PYPATH:$PATH\"" >> /root/.bashrc