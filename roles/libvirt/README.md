Role Name
=========

WARNING: For Developing and Prototyping Installs!

Uses and useful task for libvirt, QEMU and KVM on a host.

Requirements
------------

Some task need sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Create on the host

Run the main.yml task for this role. 

```
- name: Manage VM on KVM hypervisior
  hosts: "{{ playbook_groups | default('kvm_hosts') }},!grp_disabled"
  vars:
    ssh_pub_key: "Get Keys"
    machines: "{{ vm_guests }}"

  tasks:
    - name: Update package manager
      ansible.builtin.import_role:
        name: munnox.bootstrap.libvirt
        tasks_from: main.yml

```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET