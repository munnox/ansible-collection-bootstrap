Role Name
=========

WARNING: For Developing and Prototyping Installs!

Manage AWS resource first EC2 Instances.

Requirements
------------

Needs python aws Boto library and a login in aws cli to cache the credentials

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Mange AWS EC2 instances for run state and type

Run the main.yml task for this role. 

```
- name: Manage aws instances
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - name: Manage AWS EC2
      import_role:
        name: munnox.bootstrap.aws
      vars:
        # Instance in the group below need these variable defining:
        # aws_instance_id: <ec2_instance_id>
        # aws_state: running
        # aws_type: t3a.micro
        # inventory group of instances
        aws_ec2_group_name: 'grp_cloud_aws'
        
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET