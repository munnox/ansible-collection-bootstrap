Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install nix on a host. Can be for single or multiuser.
Currently installing for single user as it is more stable and doesn't require user interaction.

Requirements
------------

Needs sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install nix on the host

Run the main.yml task for this role. 

```
- name: Install NIX on hosts
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - name: Install NIX
      import_role:
        name: munnox.bootstrap.nix
      vars:
        # nix_mode: 'multiuser'
        # nix_upgrade: false
        # Ability to set the hash incase on updated install script can be overridded by the user
        nix_hash: 59bdd4f890c8dfdf8e530794bf6bf50392b6d109d772da0d953c50e6bebe34c1
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET