# Role Name

Immported from Home Collection after making it more generic and it has not been tested in a while...

It was based on this repo to build the compose file https://github.com/deviantony/docker-elk

At the tome i needed a secure setup where all endpoints had ssl certs.

So made that as a overlay layer in my fork overlay repo https://github.com/munnox/docker-elk.git 

I built the secure version on munnox-secure branch and resynced the main branch up to the main repo. I have not been maintaining the repo for a while as its has not been needed and the original repo has got better in this regard. So I am planning to move all interesting stuff into this role and modify the main line. So it has greatest applicability

Documention of elastic search

- https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-put-mapping.html

```
PUT /logstash/_mapping
{
  "properties": {
    "email": {
      "type": "keyword"
    }
  }
}
```

```
curl -XPUT "https://esa:9200/logstash/_mapping" -H 'Content-Type: application/json' -d'{"properties": {"email": {"type": "keyword"} }}'
```

## Requirements

Git, Docker and Docker Compose

## Role Variables

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

## Dependencies

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

## License

GPLv3

## Author Information

A Work in progress...
