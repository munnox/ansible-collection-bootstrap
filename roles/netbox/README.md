Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install netbox on a host.

Using:

* Docs - https://docs.netbox.dev/en/stable/
* Repo - https://github.com/netbox-community/netbox

* Installation - https://docs.netbox.dev/en/stable/installation/

Requirements
------------

Needs sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install nix on the host

Run the main.yml task for this role. 

```
- name: Install netbox on hosts
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - name: Install NIX
      import_role:
        name: munnox.bootstrap.nix
      vars:
        nix_mode: 'multiuser'
        nix_upgrade: false
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET