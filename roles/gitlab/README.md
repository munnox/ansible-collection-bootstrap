Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install gitlab on a host.

Updating path information - https://docs.gitlab.com/ee/update/index.html#upgrade-paths

Using:

* Costing - https://about.gitlab.com/pricing/premium/?step=3#overview
* EE or CE Types - https://about.gitlab.com/install/ce-or-ee/
* Docs - https://about.gitlab.com/install/#ubuntu
* Install Script - https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh

* Installation - https://about.gitlab.com/install/#ubuntu
* Install - https://packages.gitlab.com/gitlab/gitlab-ce/install#manual

Requirements
------------

Needs sudo access to the host. Add `python-gitlab` > 4.0.0 on the localhost

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install gitlab on the host

Run the `main.yml` task for this role. 

```
- name: Install gitlab on hosts
  hosts: "{{ playbook_groups | default('grp_gitlab') }},!grp_disabled"
  tasks:
    - name: Install GitLab
      ansible.builtin.import_role:
        name: munnox.bootstrap.gitlab
      vars:
        gitlab_fqdn: "{{ gitlab.fqdn }}"
        gitlab_protocol: "{{ gitlab.protocol }}"
        gitlab_certs_input: "{{ gitlab.certs }}"
        gitlab_runners: "{{ gitlab.runners | default({}) }}"
        gitlab_sso_auth: "{{ gitlab.sso_auth }}"
        # Can be defined here to be used for later sections of the install
        # gitlab_root_password: "{{ gitlab.root_pasword | default(undefined) }}"
        # If this is not defined then it can use the root user and password to create a temp one
        gitlab_admin_api_token: "{{ gitlab.api_token | default(omit) }}"
        # gitlab_hold: "{{ gitlab.package.hold | default(false) }}" # Default to not help to keep updated with latest version
        # gitlab_version: "{{ gitlab.packages.version | default(undefined) }}" # if pinned version is not needed or user wants to roll to the latest
        # Backup Options
        gitlab_backup_enabled: "{{ gitlab.backup.enable }}"
```

Or Using the collection playbook for the role

```
- name: Install gitlab
  tags: gitlab
  ansible.builtin.import_playbook: munnox.bootstrap.services.gitlab_role.yml
  vars:
    playbook_groups: grp_gitlab
    gitlab_fqdn: "{{ gitlab.fqdn }}"
    gitlab_protocol: "{{ gitlab.protocol }}"
    gitlab_certs_input: "{{ gitlab.certs }}"
    gitlab_runners: "{{ gitlab.runners }}"
    gitlab_sso_auth: "{{ gitlab.sso_auth }}"
    gitlab_admin_api_token: "{{ gitlab.api_token }}"
    gitlab_version: "{{ gitlab.packages.version }}"
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET