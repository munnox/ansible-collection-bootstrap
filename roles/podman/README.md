Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install a podman service on a host. 

Further information:

* https://podman.io/
* https://github.com/containers/image/blob/main/docs/containers-registries.conf.5.md

Requirements
------------

Needs sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None as of yet

Example Playbook
----------------

Install podman:

```
- name: Install podman on a Host
  hosts: "{{ playbook_groups | default('podman') }},!grp_disabled"
  tasks:
    - name: Install podman on host with sudo
      import_role:
        name: munnox.bootstrap.podman
      vars:
        podman_state: present
        username: "{{ ansible_user_id | default('devops') }}"
```

```
- name: Run the podman role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  roles:
      - { role: podman, state: present, username: ubuntu }
```

```
- name: Run the podman role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  # gather_facts: yes
  # gather_subset: "min"
  roles:
    - import_role:
        name: munnox.home.podman
        state: present
        username: ubuntu
```

Uninstall podman:

```
- name: Remove the podman role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  roles:
      - { role: podman, state: absent }
```

Inventory

```
---
all:
  hosts:
  children:
    podman_hosts:
      hosts:
        node01:
        node02:
```

License
-------

BSD

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET
