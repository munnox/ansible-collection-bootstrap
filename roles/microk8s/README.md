Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install a Microk8s service on a host. Includes a Dashboard by default and reports the token back on the logs.

Gets the from a Snap store for now and sets it up for the logged in user for testing.

Addons can be install based on the `addon` list.

Addons include:

* helm
* k9s
* kubectl
* dashboard <- Comes with an `admin-user` serviceAccount and clusterbinding and a LTS token to beable to see all the namespace

To get the admin-user LTS token:

```
kubectl -n kube-system describe secret/admin-user-secret
```

Requirements
------------

Needs sudo access to the host and swap disabled.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install microk8s on a server.

Run the `main.yml` task for this role. 

there are Utility tasks files as noted below these are helpers.

```yaml
- name: Run the microk8s role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  roles:
    - import_role:
        name: munnox.bootstrap.microk8s
        microk8s_state: "{{ microk8s_state | default('present') }}"
```

```yaml
- name: Install microk8s on Hosts
  hosts: "{{ playbook_groups | default('microk8s') }},!grp_disabled"
  tasks:
    - name: Install microk8s on host with sudo and noswap
      import_role:
        name: munnox.bootstrap.microk8s
```

```yaml
- name: Install microk8s on Hosts with addons
  hosts: "{{ playbook_groups | default('microk8s') }},!grp_disabled"
  tasks:
    - name: Install microk8s on host with sudo and noswap
      import_role:
        name: munnox.bootstrap.microk8s
      vars:
        microk8s_fqdn: k8s.example.com
        # microk8s_addon_dashboard_nodeport_external_port: 30001
        microk8s_addons:
          - dashboard
          - hardened
          - kubectl
          - k9s
          - helm
  
```

```yaml
- name: Get Microk8s config and Keys
  hosts: localhost
  gather_facts: false
  tasks:

    # Collects config from the group given and creates a fact called
    # microk8s_kube_config_all
    # This contains all the collected config info in a config file structure
    - name: Collection kube config from microk8s
      vars:
        microk8s_inventory_group: "{{ groups['grp_microk8s'] }}"
      ansible.builtin.import_role:
        name: munnox.bootstrap.microk8s
        tasks_from: utilities/collect_kube_config

    - name: Show cluster config 
      ansible.builtin.debug:
        msg: |
          {{ (microk8s_kube_config_all) | to_nice_yaml }}
```


```yaml
- name: Get Microk8s Dashboard config
  hosts: localhost
  vars:
    username: admin-user-token
  tasks:

    # Collects config from the group and username given and creates a fact called
    # microk8s_cluster_config_dashboard
    # This contains all the collected config info in a config file structure
    - name: Collection kube config from microk8s
      vars:
        microk8s_inventory_group: "{{ groups['grp_microk8s'] }}"
        microk8s_username_group: "{{ [username] }}"
      ansible.builtin.import_role:
        name: munnox.bootstrap.microk8s
        tasks_from: utilities/collect_kube_dashboard

    - name: Show cluster config 
      ansible.builtin.debug:
        msg: |
          {{ (microk8s_cluster_config_dashboard) | to_nice_yaml }}
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET
