Role Name
=========

Common task files are grouped here for inclusion else where. These can be run on their own providing some common utilities.

Summaries of their use can be found below:

* main.yml - Main Common role task file currently it is a placeholder with debug information
* utility_gui_xrdp.yml - Install a simple GUI and run the XRDP service for a remote desktop machine
* utility_ping_summary.yml - Print some useful information about each host as it is pinged.
* utility_debug_vars.yml - Useful host debug printed information like uptime, host group information and hostvaribles.
* utility_get_memory.yml - Gather hardware information and print the hosts gathered info and free memory.
* utility_get_hostfacts.yml - Gather facts and print hostvars for the host
* utility_get_user_ids.yml - Get the UID in "{{ user_uid }}" and GID "{{ user_gid }}" facts for a username in the varible "{{ username }}".
* utility_list_files.yml - Run "ls" in a directory capturing and printing the result.
* utility_update_package_manager.yml - Update the packages on ubuntu
* utility_download_cloud.yml - Download cloud images to a download_dir path
* utility_download_iso.yml - Download iso images to a download_dir path
* utility_helm_install.yml - Download and install helm

Requirements
------------

None

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Please see each utiliy file for examples playbook of their use.

* `tasks/utility_gui_xrdp.yml`
* `tasks/utility_ping_summary.yml`
* `tasks/utility_update_package_manager.yml`
* `tasks/utility_list_files.yml`
* `tasks/utility_get_memory.yml`
* `tasks/utility_download_cloud.yml`
* `tasks/utility_download_iso.yml`

There are other check `tasks/` for a full list most have a header with a playbook example.

The above utilties are individual Common role tasks which are used in the form as below:

```yaml
- name: Run utility ping summary task in munnox.bootstrap.common role for playbook_group={{ playbook_group | default('localhost') }}
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - name: Run utility task file for the ping summary
      import_role:
        name: munnox.bootstrap.common
        tasks_from: utility_ping_summary.yml
```

Run the main.yml task for this role. This is the default task list but it is mainly a place holder at present to test variables.

```
- name: Run the common role (Runs main.yml tasks) for playbook_group={{ playbook_group | default('localhost') }}
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  roles:
    - import_role:
      name: munnox.bootstrap.common
      state: "{{ testing | default('not defined') }}"
      port: "{{ ansible_port }}"
      message: |
        testing: {{ testing }}
        {{ ansible_port }}
        {{ state }}
```


License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET