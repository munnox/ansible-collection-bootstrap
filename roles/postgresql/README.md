Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install Postgresql service on a host.
With PGAdmin service


Requirements
------------

Needs sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install Postgesql on a server.

Run the main.yml task for this role. 

```
- name: Run the postgresql role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - ansible.builtin.import_role:
        name: munnox.bootstrap.postgresql
      vars:
        postgresql_state: "{{ postgresql_state | default('present') }}"
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET
