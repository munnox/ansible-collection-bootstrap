Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install Airflow service on a host. Uses the Airflow HELM chart.

HELM Chart Configuration parameters can be found https://airflow.apache.org/docs/helm-chart/stable/parameters-ref.html


Requirements
------------

Needs sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install airflow on a server.

Run the main.yml task for this role. 

```
- name: Run the airflow role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - ansible.builtin.import_role:
        name: munnox.bootstrap.airflow
      vars:
        airflow_state: "present"
        airflow_fqdn: "airflow.example.com"
        # airflow_git_sync_repo: "https://github.com/apache/airflow.git"
        # airflow_git_sync_branch: main
        # airflow_git_sync_subpath: dags
        airflow_extra_env:
          - name: AIRFLOW_CONN_DEV_SERVER
            value: "ssh://user@dev.example.com:22?conn_timeout=30&compress=false&no_host_key_check=true&allow_host_key_change=true&key_file=~/.ssh/id_rsa"
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET
