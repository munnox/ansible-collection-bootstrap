Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install a K3s service on a host. Includes a Dashboard by default and reports the token back on the logs.

Gets the install script from https://get.k3s.io and verifys it via hash... This is mostly ok but the website always links to the latest and it need pinning for consistancy.

Requirements
------------

Needs sudo access to the host and swap disabled.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install k3s on a server.

Run the main.yml task for this role. 

```
- name: Run the k3s role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - ansible.builtin.import_role:
        name: munnox.bootstrap.k3s
      vars:
        k3s_state: "{{ k3s_state | default('present') }}"
```


License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET
