Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install Unifi on a host.
Currently installing Unifi and mongoDB Repos and installing the main package.

Service Available on https://<IPaddress>:8443/

Requirements
------------

Needs sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install Unifi Controller on the host

Run the main.yml task for this role. 

```
- name: Install Unifi on hosts
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - name: Install Unifi
      import_role:
        name: munnox.bootstrap.unifi
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET