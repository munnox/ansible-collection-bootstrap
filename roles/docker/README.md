Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install a docker service on a host. 

Further information:

* https://docs.docker.com/config/daemon/

Requirements
------------

Needs sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None as of yet

Example Playbook
----------------

Install docker:

```
- name: Install Docker on a Host
  hosts: "{{ playbook_groups | default('docker') }},!grp_disabled"
  tasks:
    - name: Install Docker on host with sudo
      import_role:
        name: munnox.bootstrap.docker
      vars:
        docker_state: present
        username: "{{ ansible_user_id | default('devops') }}"
```

```
- name: Run the docker role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  roles:
      - { role: docker, state: present, username: ubuntu }
```

```
- name: Run the docker role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  # gather_facts: yes
  # gather_subset: "min"
  roles:
    - import_role:
        name: munnox.home.docker
        state: present
        username: ubuntu
```

Uninstall docker:

```
- name: Remove the docker role (Runs main.yml tasks)
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  roles:
      - { role: docker, state: absent }
```

Inventory

```
---
all:
  hosts:
  children:
    docker_hosts:
      hosts:
        node01:
        node02:
```

License
-------

BSD

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET
