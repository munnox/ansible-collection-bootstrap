Role Name
=========

WARNING: For Developing and Prototyping Installs!

Install Ceph on a host.
Currently installing CephADM for playing and setting various Ceph setups

Requirements
------------

Needs sudo access to the host.

Role Variables
--------------

See the  `default/main.yml` for the list of defined variables used in the role with description listed with them.

Dependencies
------------

None

Example Playbook
----------------

Install Ceph on the host

Run the main.yml task for this role. 

```
- name: Install Ceph on hosts
  hosts: "{{ playbook_groups | default('localhost') }},!grp_disabled"
  tasks:
    - name: Install Ceph
      import_role:
        name: munnox.bootstrap.ceph
      vars:
        ceph_version: 'quincy'
```

License
-------

GPLv3

Author Information
------------------

By Dr Robert Munnoch PhD Meng MIET