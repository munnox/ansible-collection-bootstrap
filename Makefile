init:
	poetry install --no-root

build_docker:
	ansible-builder create --output-filename Dockerfile 
	docker build --platform linux/amd64 -f context/Dockerfile context -t dev_bootstrap_ee:latest

dev_docker:
	docker run -it --rm -v $(shell pwd):/runner --hostname dev_bootstap --platform linux/amd64 dev_bootstrap_ee:latest

build_podman:
	# poetry run ansible-builder create --output-filename Containerfile 
	ansible-builder create --output-filename Containerfile 
	podman build --platform linux/amd64 -f context/Containerfile context -t dev_bootstrap_ee:latest

dev_podman:
	podman run -it --rm -v $(shell pwd):/workspace --platform linux/amd64 dev_bootstrap_ee:latest

export_pip:
	# poetry config warnings.export false
	poetry export --without-hashes --format=requirements.txt > requirements.txt