import datetime
import os
import subprocess
from ansible.module_utils._text import to_bytes, to_native, to_text
from ansible.errors import AnsibleError, AnsibleFilterError, AnsibleFilterTypeError
from ansible.utils.display import Display
from ansible.parsing.yaml.dumper import AnsibleDumper
import yaml
import uuid
from textwrap import dedent
import base64


from jinja2.filters import pass_environment

display = Display()

UUID_NAMESPACE_ANSIBLE = uuid.UUID("6BE05C78-535F-41CC-82E1-B1BF91F3C8E9")

# NOTE: Might be in the wrong place but placed here as it is still useful
DOCUMENTATION = """
- name: Bitwarden Interface
  version_added: '0.1.0'
  short_description: Fetch a Bitwarden Record using the BW CLI to a lookup with account password.
  description:
    - A Bitwarden Interface to access secrets and password stored in Bitwarden collections
    - for use in Ansible playbooks to gather sensative information.
  notes:
    - A simple lookup wit reduce the base dependances.
  options:
    _bw_password:
      description: A BW Password to unlock vault.
      type: string
      required: true
    _record_id:
      description: A record ID to allow secrets to be retrived by uuid.
      type: string
      required: true
EXAMPLES: |
  {{ None | munnox.bootstrap.bw_id(pw_password, record_id)}}
RETURN:
  _value:
    description: The chosen bitwarden records.
    type: raw
"""

from ansible_collections.munnox.bootstrap.plugins.module_utils.bitwarden import (
    Bitwarden_CLI,
)

@pass_environment
def munnox_bitwarden_session_id(environment, filter_in, *args, **kw):
    """Retreive bitwarden session using the bw cli."""
    display.debug(f"Bitwarden session id filter running")
    # display.debug(f"Env: {os.environ} KW: {kw}")
    start = datetime.datetime.now()

    client_id = None
    if 'client_id' in kw:
        client_id = kw['client_id']
    client_secret = None
    if 'client_secret' in kw:
        client_secret = kw['client_secret']
    password = None
    if 'password' in kw:
        password = kw['password']

    try:
        session = Bitwarden_CLI.bw_login_api(client_id, client_secret, password=password)
    except Exception as error:
        display.error(f"Bitwarden session id filter Failed. Error: {error} ")
        raise error
        

    end = datetime.datetime.now()
    span = (end - start).total_seconds()
    display.debug(f"Bitwarden session id filter done. Took: {span:0.1f}s ")

    result = [{"session_id": session['session_id']}]
    return result


@pass_environment
def munnox_bitwarden_interface_ids(
    environment, record_ids, session_id=None, *args, **kw
):
    """Retreive bitwarden secrets using the bw cli."""
    start = datetime.datetime.now()
    from_json = environment.filters["from_json"]
    to_json = environment.filters["to_json"]
    # default_flow_style = kw.pop('default_flow_style', None)
    # try:
    #     transformed = yaml.dump(a, Dumper=AnsibleDumper, allow_unicode=True, default_flow_style=default_flow_style, **kw)
    # except Exception as e:
    #     raise AnsibleFilterError("to_yaml - %s" % to_native(e), orig_exc=e)
    # bw get attachment photo.png --itemid 99ee88d2-6046-4ea7-92c2-acac464b1412 --output /Users/myaccount/Pictures/
    keywords = dict(kw)
    timeout_sync = 30
    if "timeout_sync" in keywords:
        timeout_sync = keywords.pop("timeout_sync")
    debug = False
    if "debug" in keywords:
        debug = keywords.pop("debug")

    display.debug(f"Bitwarden filter running")

    if session_id is None:
        session_id = os.environ.get('BW_SESSION', None)
    
    if session_id is None:
        raise AnsibleFilterError("Bitwarden Lookup has no Session ID Given. Use 'session_id' to set one at runtime or BW_SESSION environment variable")  

    try:
        # display.v(f"Bitwarden Unlock running")
        # session_id = Bitwarden_CLI.bw_api_login(password)
        # session_id = Bitwarden_CLI.bw_unlock(password)
        # display.v(f"Bitwarden Sync running")
        # Bitwarden_CLI.bw_sync(session_id, timeout=timeout_sync)
        list_records = False
        if record_ids is None:
            list_records = True

        if list_records:
            display.debug(f"Bitwarden List Records running")
            try:
                records = Bitwarden_CLI.bw_list_records(session_id=session_id)
            except Exception as error:
                raise AnsibleFilterError(error)

        else:
            display.debug(f"Bitwarden Get Record running")
            records = []
            for record_id in record_ids:
                display.debug(f"Bitwarden Get Record id:'{record_id}'")
                try:
                    record = Bitwarden_CLI.bw_get_record(
                        session_id=session_id, record_id=record_id
                    )
                    records.append({
                        "item": record_id,
                        "record": record
                    })
                except Warning as error:
                    display.warning(error)
                    # raise AnsibleFilterError(error)
                except Exception as error:
                    raise AnsibleFilterError(error)
        # display.vvv(f"Result: {result}")

    except AnsibleFilterError:
        raise
    except Exception as e:
        raise AnsibleFilterError(
            f"Bitwarden filter failed type: {type(e)} - {to_native(e)} record_id: {record_id}",
            orig_exc=e,
        )
    end = datetime.datetime.now()
    span = (end - start).total_seconds()
    display.v(f"Bitwarden filter done. ids:'{record_ids}' Took: {span:0.1f}s or {span/len(record_ids):0.2f}s per record.")
    # result = {
    #     "record_ids": record_ids,
    #     "records": records,
    # }
    # if debug:
    #     result["debug"] = {
    #         "test": "mvp",
    #         "args": args,
    #         "kw": kw,
    #     }
    # results = [result]

    # display.v(f"Result: {results}")
    return records


@pass_environment
def munnox_bitwarden_interface_terms(
    environment, record_terms, session_id=None, *args, **kw
):
    """Retreive bitwarden secrets using the bw cli."""
    start = datetime.datetime.now()
    from_json = environment.filters["from_json"]
    to_json = environment.filters["to_json"]
    # default_flow_style = kw.pop('default_flow_style', None)
    # try:
    #     transformed = yaml.dump(a, Dumper=AnsibleDumper, allow_unicode=True, default_flow_style=default_flow_style, **kw)
    # except Exception as e:
    #     raise AnsibleFilterError("to_yaml - %s" % to_native(e), orig_exc=e)
    # bw get attachment photo.png --itemid 99ee88d2-6046-4ea7-92c2-acac464b1412 --output /Users/myaccount/Pictures/
    keywords = dict(kw)
    timeout_sync = 30
    if "timeout_sync" in keywords:
        timeout_sync = keywords.pop("timeout_sync")
    debug = False
    if "debug" in keywords:
        debug = keywords.pop("debug")

    display.debug(f"Bitwarden filter running")
    
    full = False
    if "full" in keywords:
        full = keywords.pop("full")

    if session_id is None:
        session_id = os.environ.get('BW_SESSION', None)
    
    if session_id is None:
        raise AnsibleFilterError("Bitwarden Lookup has no Session ID Given. Use 'session_id' to set one at runtime or BW_SESSION environment variable")  

    try:
        # display.v(f"Bitwarden Unlock running")
        # session_id = Bitwarden_CLI.bw_api_login(password)
        # session_id = Bitwarden_CLI.bw_unlock(password)
        # display.v(f"Bitwarden Sync running")
        # Bitwarden_CLI.bw_sync(session_id, timeout=timeout_sync)
        list_records = False
        if record_terms is None:
            list_records = True

        if list_records:
            display.debug(f"Bitwarden List Records running")
            try:
                records = Bitwarden_CLI.bw_list_records(session_id=session_id, full=full)
            except Exception as error:
                raise AnsibleFilterError(error)

        else:
            display.debug(f"Bitwarden Search Records running")
            records = []
            for search_term in record_terms:
                display.debug(f"Bitwarden Search Record term: '{search_term}'")
                try:
                    search_records = Bitwarden_CLI.bw_list_records(
                        session_id=session_id,
                        search_term=search_term,
                        full=full
                    )
                    display.debug(f"Bitwarden Search Record results: '{search_records}'")
                    records.append({
                        "item": search_term,
                        "records": search_records
                    })
                except Warning as error:
                    display.warning(error)
                    # raise AnsibleFilterError(error)
                except Exception as error:
                    raise AnsibleFilterError(error)
        # display.vvv(f"Result: {result}")

    except AnsibleFilterError:
        raise
    except Exception as e:
        raise AnsibleFilterError(
            f"Bitwarden filter failed type: {type(e)} - {to_native(e)} record_id: {search_term}",
            orig_exc=e,
        )
    end = datetime.datetime.now()
    span = (end - start).total_seconds()
    display.v(f"Bitwarden filter done. Terms: '{record_terms}' Took: {span:0.1f}s ")
    # result = {
    #     "record_terms": record_terms,
    #     "records": records,
    # }
    # if debug:
    #     result["debug"] = {
    #         "test": "mvp",
    #         "args": args,
    #         "kw": kw,
    #     }
    # results = [result]

    # display.v(f"Result: {results}")
    return records

@pass_environment
def munnox_bitwarden_interface_template(
    environment, template_terms, session_id, *args, **kw
):
    """Retreive bitwarden secrets using the bw cli."""
    start = datetime.datetime.now()
    from_json = environment.filters["from_json"]
    to_json = environment.filters["to_json"]

    
    # default_flow_style = kw.pop('default_flow_style', None)
    # try:
    #     transformed = yaml.dump(a, Dumper=AnsibleDumper, allow_unicode=True, default_flow_style=default_flow_style, **kw)
    # except Exception as e:
    #     raise AnsibleFilterError("to_yaml - %s" % to_native(e), orig_exc=e)
    # bw get attachment photo.png --itemid 99ee88d2-6046-4ea7-92c2-acac464b1412 --output /Users/myaccount/Pictures/
    keywords = dict(kw)
    timeout_sync = 30
    if "timeout_sync" in keywords:
        timeout_sync = keywords.pop("timeout_sync")
    debug = False
    if "debug" in keywords:
        debug = keywords.pop("debug")
    display.debug(f"Bitwarden filter running")

    if session_id is None:
        session_id = os.environ.get('BW_SESSION', None)
    
    if session_id is None:
        raise AnsibleFilterError("Bitwarden Lookup has no Session ID Given. Use 'session_id' to set one at runtime or BW_SESSION environment variable")  

    try:
        # display.v(f"Bitwarden Unlock running")
        # session_id = Bitwarden_CLI.bw_api_login(password)
        # session_id = Bitwarden_CLI.bw_unlock(password)
        # display.v(f"Bitwarden Sync running")
        # Bitwarden_CLI.bw_sync(session_id, timeout=timeout_sync)

        display.debug(f"Bitwarden Get Record Template running")
        records = []
        for template_term in template_terms:
            display.debug(f"Bitwarden Get Record template {template_term}")
            try:
                record = Bitwarden_CLI.bw_get_template(
                    session_id=session_id, template=template_term
                )
                # records.append(record)
                records.append({
                    "item": template_term,
                    "template": record
                })
            # except Warning as error:
            #     display.warning(error)
            #     # raise AnsibleFilterError(error)
            except Exception as error:
                raise AnsibleFilterError(error)
        # display.vvv(f"Result: {result}")

    except AnsibleFilterError:
        raise
    except Exception as e:
        raise AnsibleFilterError(
            f"Bitwarden filter failed type: {type(e)} - {to_native(e)} record_id: {template_term}",
            orig_exc=e,
        )
    end = datetime.datetime.now()
    span = (end - start).total_seconds()
    display.v(f"Bitwarden filter done. Template '{template_terms}' Took: {span:0.1f}s ")

    # result = {
    #     "record_ids": record_ids,
    #     "records": records,
    # }
    # if debug:
    #     result["debug"] = {
    #         "test": "mvp",
    #         "args": args,
    #         "kw": kw,
    #     }
    # results = [result]

    # display.v(f"Result: {results}")
    return records

class FilterModule(object):
    """Ansible core jinja2 filters"""

    def filters(self):
        return {
            "bitwarden_session": munnox_bitwarden_session_id,
            "bitwarden_get_ids": munnox_bitwarden_interface_ids,
            "bitwarden_get_terms": munnox_bitwarden_interface_terms,
            "bitwarden_template": munnox_bitwarden_interface_template,
            # debug
            "type_debug": lambda o: o.__class__.__name__,
        }
