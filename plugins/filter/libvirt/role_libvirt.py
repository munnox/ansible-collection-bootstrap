from ansible.module_utils._text import to_bytes, to_native, to_text
from ansible.errors import AnsibleError, AnsibleFilterError, AnsibleFilterTypeError
from ansible.utils.display import Display
from ansible.parsing.yaml.dumper import AnsibleDumper
import yaml
import uuid
from textwrap import dedent

# https://peps.python.org/pep-0386/#distutils
from distutils.version import StrictVersion as V

from jinja2.filters import pass_environment, pass_context

display = Display()

UUID_NAMESPACE_ANSIBLE = uuid.UUID("BE159A87-65C5-426C-8795-40F611FF3656")

# NOTE: Might be in the wrong place but placed here as it is still useful
DOCUMENTATION = """
- name: machine_definition
  version_added: '0.1.0'
  short_description: Convert VM List of infomation into a built template and virsh command
  description:
    - Converts a VM information variable into a vm build config with paths and command for the realisation.
    - This expands the VM information and config into a something that then could be realised.
  notes:
    - This is used in the KVM playbook and soon the libvirt role and create_vm_task list.
  options:
    _machines:
      description: A list of dictionaries with the main config of a vm.
      type: list
      required: true
    _virt_uri:
      description: A list of dictionaries with the main config of a vm.
      type: list
      required: true
    _image_dir:
      description: A list of dictionaries with the main config of a vm.
      type: list
      required: true
    _download_dir:
      description: A list of dictionaries with the main config of a vm.
      type: list
      required: true
EXAMPLES: |
  # vm_guests:
  #   - name: testing
  #     state: present
  #     domain: example.com
  #     disk_type_ext: qcow2
  #     cpus: 2
  #     memory_gib: 2
  #     storage_size: 20G
  #     network_connection: bridge=bridge_default
  #     restart_needed: false
  #     # snapshot_backup_name: "test1"
  #     snapshot_backup_name: false
  #     # snapshot_restore_name: "test1"
  #     snapshot_restore_name: false
  #     image_name: jammy-server-cloudimg-amd64.img
  #     ssh_key_pub: "{{ ssh_key_pub }}"
  #     user: ubuntu
  #     addresses: 192.168.0.2/24

  #   - name: rocky
  #     state: present
  #     domain: example.com
  #     disk_type_ext: qcow2
  #     cpus: 2
  #     memory_gib: 2
  #     storage_size: 20G
  #     network_connection: bridge=bridge_default
  #     restart_needed: false
  #     # snapshot_backup_name: "test1"
  #     snapshot_backup_name: false
  #     # snapshot_restore_name: "test1"
  #     snapshot_restore_name: false
  #     image_name: Rocky-9-GenericCloud-9.0-20220830.0.x86_64.qcow2
  #     ssh_key_pub: "{{ ssh_key_pub }}"
  #     user: rocky
  #     addresses: 192.168.0.3/24
  #
  # virt_uri: qemu:///system
  # image_dir: /var/lib/libvirt/images
  # download_dir: "{{ ansible_user_dir }}"
  #
  {{ vm_guests | machine_definition(virt_uri, image_dir, download_dir)}}
RETURN:
  _value:
    description: The variable resulting from realising the VM build.
    type: raw
"""


@pass_environment
def munnox_machine_definition(
    environment, machines, virt_uri, image_dir, base_image_dir, *args, **kw
):
    """Define VM's"""
    # default_flow_style = kw.pop('default_flow_style', None)
    # try:
    #     transformed = yaml.dump(a, Dumper=AnsibleDumper, allow_unicode=True, default_flow_style=default_flow_style, **kw)
    # except Exception as e:
    #     raise AnsibleFilterError("to_yaml - %s" % to_native(e), orig_exc=e)
    default_virt_install_version = kw.pop("virt_install_version", "4.0.0")
    virt_install_version = V(default_virt_install_version)

    # display.warning(f"virt-install version: {virt_install_version} {V('2.0.0') <= virt_install_version} {virt_install_version < V('3.0.0')}")
    # to_yaml = environment.filters['to_yaml']
    # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/version_test.html
    # V = environment.filters['version']



    vms = []
    for vm in machines:
        seed_path = f"{ image_dir }/{ vm['name'] }-seed.img"
        main_disk_path = f"{ image_dir }/{ vm['name'] }.{ vm['disk_type_ext'] }"
        # print(vm)

        vi_cmd_list = [
            "virt-install",
            f"--connect { virt_uri }",
            f"--name { vm['name'] }",
            # "--virt-type kvm",
            f"--memory { vm['memory_gib'] * 1024 }",
            f"--vcpus { vm['cpus'] }",
            "--cpu host-passthrough",
            "--boot hd,menu=on",
            f"--disk path={ seed_path },device=cdrom",
            f"--disk path={ main_disk_path },device=disk",
            "--graphics vnc",
            f"--network { vm['network_connection'] }",
            "--console pty,target_type=serial",
            "--noautoconsole"
        ]
        if (V('2.0.0') <= virt_install_version) and (virt_install_version < V('3.0.0')):
            vi_cmd_list += [
                "--os-type Linux --os-variant ubuntu20.04", # for version 2.2.1
            ]
        if (V('4.0.0') <= virt_install_version) and (virt_install_version < V('4.1.0')):
            vi_cmd_list += [
                "--osinfo linux2022", # for version 4.0.0
            ]
        # If defined add the autostart option to the VM install command
        if vm['autostart']:
            vi_cmd_list += [
                "--autostart",
            ]


        vm_built = {
            "vm": vm,
            "image_path": f"{ base_image_dir }/{ vm['image_name'] }",
            "main_disk_path": main_disk_path,
            "seed_path": seed_path,
            "cloud_config_path": f"{ base_image_dir }/{ vm['name'] }_cloud_init.cfg",
            "network_config_path": f"{ base_image_dir }/{ vm['name'] }_snetwork_config_static.cfg",
            "virt_install_version": str(virt_install_version),
            "virsh_cmd": " \\\n ".join(vi_cmd_list)
            # --os-type Linux --os-variant ubuntu20.04 \
            # ).strip(" \n"),
            # "virsh_cmd_default": """
            #   virt-install --connect {{ virt_uri }} --name {{ vm.name }} \
            #     --virt-type kvm --memory {{ new_memory_mib }} --vcpus 2 \
            #     --boot hd,menu=on \
            #     --disk path={{ seed_path }},device=cdrom \
            #     --disk path={{ main_disk_path }},device=disk \
            #     --graphics vnc \
            #     --os-type Linux --os-variant ubuntu20.04 \
            #     --network network:default \
            #     --console pty,target_type=serial \
            #     --wait 0
            # """,
        }
        vms.append(vm_built)
    return vms

def build_image_name(image_info):
    return image_info['name'] + '-' + image_info['image']['checksum_sha256'][:5] + "-" + image_info['image']['image_name']

def munnox_libvirt_build_image_name(image_info, *args, **kw):
    return build_image_name(image_info)

# # Further information https://docs.ansible.com/ansible/latest/plugins/inventory/yaml.html
# Placed Old VM image flavor information here for time being Depreciated but maybe useful
# x-VM_Images:
#   hosts:
#     x-ubuntu2204-vm: &ubuntu2204vm
#       # name: dev
#       # state: absent
#       user: ubuntu
#       # addresses: 192.168.0.1/24
#       # cpus: 2
#       # memory_gib: 4
#       network_connection: bridge=bridge.1
#       domain: example.com
#       disk_type_ext: qcow2
#       storage_size: 20G
#       restart_needed: false
#       # snapshot_backup_name: "test1"
#       snapshot_backup_name: false
#       # snapshot_restore_name: "test1"
#       snapshot_restore_name: false
#       image_name: jammy-server-cloudimg-amd64.img
#       ssh_key_pub: "{{ lookup('ansible.builtin.file', '~/.ssh/id_ed25519.pub') }}"

#     x-ubuntu2004-vm: &ubuntu2004vm
#       # name: dev
#       # state: absent
#       user: ubuntu
#       # addresses: 192.168.0.2/24
#       # cpus: 2
#       # memory_gib: 4
#       network_connection: bridge=bridge.1
#       domain: example.com
#       disk_type_ext: qcow2
#       storage_size: 20G
#       restart_needed: false
#       # snapshot_backup_name: "test1"
#       snapshot_backup_name: false
#       # snapshot_restore_name: "test1"
#       snapshot_restore_name: false
#       image_name: ubuntu-20.04-server-cloudimg-amd64.img
#       ssh_key_pub: "{{ lookup('ansible.builtin.file', '~/.ssh/id_ed25519.pub') }}"

#     x_fedora_vm: &fedoravm
#       # name: dev
#       # state: absent
#       user: fedora
#       # addresses: 192.168.0.3/24
#       # cpus: 2
#       # memory_gib: 4
#       network_connection: bridge=bridge.1
#       domain: example.com
#       disk_type_ext: qcow2
#       storage_size: 20G
#       restart_needed: false
#       # snapshot_backup_name: "test1"
#       snapshot_backup_name: false
#       # snapshot_restore_name: "test1"
#       snapshot_restore_name: false
#       image_name: Fedora-Cloud-Base-37-1.7.x86_64.qcow2
#       ssh_key_pub: "{{ lookup('ansible.builtin.file', '~/.ssh/id_ed25519.pub') }}"

#     x-rocky-vm: &rockyvm
#       # name: rocky
#       # state: absent
#       user: rocky
#       # addresses: 192.168.0.4/24
#       # cpus: 2
#       # memory_gib: 2
#       network_connection: bridge=bridge.1
#       domain: munnox.com
#       disk_type_ext: qcow2
#       storage_size: 20G
#       restart_needed: false
#       # snapshot_backup_name: "test1"
#       snapshot_backup_name: false
#       # snapshot_restore_name: "test1"
#       snapshot_restore_name: false
#       image_name: Rocky-9-GenericCloud-9.0-20220830.0.x86_64.qcow2
#       ssh_key_pub: "{{ lookup('ansible.builtin.file', '~/.ssh/id_ed25519.pub') }}"
# 
# all:
#   hosts:
#     netbox2023:
#       ansible_user: ubuntu
#       ansible_host: 192.168.0.4
#       vm_info:
#         name: netbox2023
#         # Add dict from above
#         <<: *ubuntu2004vm
#         # New way to specify type used below and from now on
#         #vm_type: ubuntu_22.04
#         state: present
#         cpus: 2
#         memory_gib: 4
#         storage_size: 50G
#         addresses: 192.168.0.4/24

def fetch_vm_template(guest, vm_images, type_key):
    if type_key in guest:
        if guest[type_key] not in vm_images.keys():
            name  = guest['name'] if 'name' in guest else guest
            raise AnsibleFilterError(
                f"VM Template fetch error in role '{__name__}' vm_info.vm_type of '{name}' must be one off - {list(vm_images.keys())}"
            )
        guest_template = vm_images[guest[type_key]]
        return guest_template
    return None


def fill_in_guest_gaps(guest, template):
    if template is None:
        return guest
    # Fill in the gaps in the guest with template
    for k in template:
        if k not in guest:
            guest[k] = template[k]
    return guest

def define_vm(context, inital_guests, hostvars, kw):
    default_hostname = kw.pop("hostname", None)
    default_guest_info_key = kw.pop("info_key", "vm_info")
    default_guest_type_key = kw.pop("type_key", "vm_type")
    default_domain = kw.pop("domain", "example.com")
    default_gateway = kw.pop("gateway", "192.168.0.1")
    default_dns = kw.pop("dns", "192.168.0.1")
    default_connection = kw.pop("connection", "default") # alt is bridge=<nameofbridge>
    default_ssh_public_keys = kw.pop("ssh_key_pub", None)

    vars = context.get_all()
    display.v(f"Running VM MAchine definition Filter")
    # print(f"Testing filter")
    # display.v(f"Vars: ${vars.keys()}")

    # NOTE: These vars are defined in the role as a base versions
    # They can be overridden by being passed to the role.
    # This means the Machine defined is only available within the role
    base = vars['base']
    images = vars['images']
    images_dict = {
        ele['name']: ele
        for ele in images
    }
    # try:
    #     transformed = yaml.dump(a, Dumper=AnsibleDumper, allow_unicode=True, default_flow_style=default_flow_style, **kw)
    # except Exception as e:
    #     raise AnsibleFilterError("to_yaml - %s" % to_native(e), orig_exc=e)
    # print("to terminal")
    # Display("test")
    # print()
    
    # Base and common vm definitions
    common_struct = {
        **base,
        "network_connection": default_connection,
        "domain": default_domain,
        "gateway": default_gateway,
        "dns": default_dns,
        "image_name": build_image_name(images_dict['ubuntu_22_04']),
        "ssh_key_pub": default_ssh_public_keys,
    }

    # display.warning(f"Running: {common_struct}")
    # display.warning(f"Running: {images_dict}")

    vm_images = {
        ele: {
            # Start with common variable
            **common_struct,
            # Layer image specific info on top of the common
            **images_dict[ele]['vm_info'],
            # **vm_images[ele],
            # Finalise the image name
            "image_name": build_image_name(images_dict[ele]),
        }
        for ele in images_dict
    }

    vm_guests = {}

    # if default_hypervisior is not None:
        # original_desk_rob = vm_guests # hostvars[default_hypervisior]["vm_guests"]
    
    error_report = []

    # first fill any inital guest vm given
    if inital_guests is not None:
        for guest in inital_guests:
            try:
                template = fetch_vm_template(guest, vm_images, default_guest_type_key)
            except AnsibleFilterError as error:
                error_report.append(f"'{vm_name}.{default_guest_info_key}.{default_guest_type_key}'={repr(guest[default_guest_type_key])} must be one off - {list(vm_images.keys())}")
                template = {}
                # raise error

            guest = fill_in_guest_gaps(guest, template)
            vm_guests[guest["name"]] = guest

    # Next loop through the Hosts in the inventory and look for
    # the 'default_guest_info_key' in the keys and fill the 'vm_info dict'
    # with template info
    for vm_name in hostvars:
        vm = hostvars[vm_name]
        if default_guest_info_key in vm:
            # continue as guest info dict key found
            guest = dict(vm[default_guest_info_key])
            

            # Pull and merge the give vm guest list for vm name and the 
            # vm info variable default_guest_info_key
            if vm_name in vm_guests:
                vm_guests[vm_name].update(guest)
            else:
                vm_guests[vm_name] = guest
            
            needed_fields = [
                'name',
                default_guest_type_key,
                'hypervisor_hostname'
            ]
            for key in needed_fields:
                if key not in guest:
                    error_report.append(
                        # f"Host with key of '{vm_name}' "
                        f"'{vm_name}.{default_guest_info_key}.{key}' - Not defined in guest info."
                    )

            # Now merged and errors reported look to see if the merge vm should
            # be included in the current hostname
            if "hypervisor_hostname" in vm_guests[vm_name]:
                # check to see if the right hypervisor is being run
                if vm_guests[vm_name]["hypervisor_hostname"] != default_hostname:
                    del vm_guests[vm_name]
                    continue
            # else:
            #     vm_guests[vm_name] = guest 

            try:
                template = fetch_vm_template(guest, vm_images, default_guest_type_key)
            except AnsibleFilterError as error:
                # error_report.append(f"{error}")
                # name  = guest['name'] if 'name' in guest else vm_name
                error_report.append(f"'{vm_name}.{default_guest_info_key}.{default_guest_type_key}'={repr(guest[default_guest_type_key])} must be one off - {list(vm_images.keys())}")
                # raise error
                template = {}

            guest = fill_in_guest_gaps(guest, template)

            
            needed_fields = [
                'name',
                default_guest_type_key,
                'hypervisor_hostname',
                'cpus',
                'memory_gib',
                'domain',
                'gateway',
                'dns',
                'network_connection',
                'storage_size',
                'disk_type_ext',
                'user',
                'image_name',
                'ssh_key_pub',
                'addresses',
                'interface_name',
                'interface_type',
                'guest_family',
                'autostart'
            ]
            for key in needed_fields:
                if key not in vm_guests[vm_name]:
                    error_report.append(
                        # f"Host with key of '{vm_name}' "
                        f"'{vm_name}.{default_guest_info_key}.{key}' - Not defined in guest info."
                    )

    if len(error_report) > 0:
        report_str = "\n".join(error_report)
        raise AnsibleFilterError(
            f"VM Template fetch error in role '{__name__}':\n{report_str}"
        )

    # vm_g_current = {v['name'] for v in original_desk_rob if v['state'] == "present"}
    # vm_g_proposed = {v['name'] for v in vm_guests if v['state'] == "present"}
    # d = [
    #     "current-proposed",
    #     vm_g_current - vm_g_proposed,
    #     "proposed-current",
    #     vm_g_proposed - vm_g_current
    # ]
    vm_guests = [vm_guests[vm] for vm in vm_guests]
    # vm_guests = [
    #     guest for guest in vm_guests
    #     if guest['state'] == 'present'
    # ]
    return vm_guests

# @pass_environment
@pass_context
def munnox_guests_definitions(context, inital_guests, hostvars, *args, **kw):
    """Define VM's"""
    # default_hypervisior = kw.pop("hypervisor", None)
    try:
        vm_guests = define_vm(context, inital_guests, hostvars, kw)
    except Exception as error:
        raise Exception(f"General Error ${error}")
    return vm_guests


class FilterModule(object):
    """Ansible core jinja2 filters"""

    def filters(self):
        return {
            "machine_definition": munnox_machine_definition,
            "vm_guest_definition": munnox_guests_definitions,
            "build_image_name": munnox_libvirt_build_image_name,
            # debug
            "type_debug": lambda o: o.__class__.__name__,
        }
