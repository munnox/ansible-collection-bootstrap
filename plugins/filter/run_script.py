# Very Basic script runner for ease of filtering and process within ansible...
# This removes large global dependances and allows them to be bundled in with the system
# WARNING this allows arbitary code execution!!!
# Use with care!!! 
# Author Robert Munnoch
# To run:
# ANSIBLE_DEBUG=true ansible-playbook -v munnox.bootstrap.examples.test_filter_script

from jinja2 import pass_environment, pass_context
from ansible.module_utils._text import to_bytes, to_native, to_text
from ansible.errors import AnsibleError, AnsibleFilterError, AnsibleFilterTypeError
from ansible.utils.display import Display
from ansible.parsing.yaml.dumper import AnsibleDumper
import yaml
import uuid
import traceback, sys

from ansible_collections.munnox.bootstrap.plugins.module_utils.run_script_filter import (
    run_script_filter,
)

# from jinja2.filters import pass_environment

display = Display()

# Almost the same just changed via 'uuidgen'
UUID_NAMESPACE_ANSIBLE = uuid.UUID("736b544a-1eb5-48e2-a63b-05538700e8d7")


# Simple filter basically does what to_yaml does from the
# core to show the principles and import system.
@pass_environment
def munnox_run_filter_script(context, filter_input, script, *args, **kw):
    """Filter that can run python scripts with args"""
    # default_flow_style = kw.pop("default_flow_style", None)
    kw['filter_input'] = filter_input
    display.debug(f"before run\n{script}")
    # display.vvv(f"before run\n{script}")
    display.debug(f"Filter Input: {filter_input}")
    args = (filter_input, *args)
    try:
        transformed = run_script_filter({"type": "filter", "context": context, "display": display}, script, *args, **kw)
    except SyntaxError as e:
        raise AnsibleFilterError("filter_script - SyntaxError %s\n%s" % (to_native(e), traceback.format_exc()), orig_exc=e)
    except NameError as e:
        raise AnsibleFilterError("filter_script - NameError %s\n%s" % (to_native(e), traceback.format_exc()), orig_exc=e)
    except Exception as e:
        raise AnsibleFilterError("filter_script - type(%s) %s" % (type(e), to_native(e)), orig_exc=e)
    return transformed #to_text(transformed)

class FilterModule(object):
    """Ansible core jinja2 filters"""

    def filters(self):
        return {
            "filter_script": munnox_run_filter_script,
            # debug
            "type_debug": lambda o: o.__class__.__name__,
        }
