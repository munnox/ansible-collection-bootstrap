import os
import subprocess
from ansible.module_utils._text import to_bytes, to_native, to_text
from ansible.errors import AnsibleError, AnsibleFilterError, AnsibleFilterTypeError
from ansible.utils.display import Display
from ansible.parsing.yaml.dumper import AnsibleDumper
import yaml
import uuid
from textwrap import dedent
# Futher info here https://gitlab.com/munnox/python-tiddlywiki
import tiddlywiki

import urllib3

from jinja2.filters import pass_environment

display = Display()
urllib3.disable_warnings()

UUID_NAMESPACE_ANSIBLE = uuid.UUID("6BE05C78-535F-41CC-82E1-B1BF91F3C8E8")

# NOTE: Might be in the wrong place but placed here as it is still useful
DOCUMENTATION = """
- name: Tiddlywiki Interface
  version_added: '0.1.0'
  short_description: Fetch a Tiddlywiki Record using my python API to a lookup with credensials and a URL.
  description:
    - A Tiddlywiki Interface to access tiddlers stored in a Tiddlywiki collections
    - for use in Ansible playbooks to gather information.
  notes:
    - A simple filter reduce the base dependances.
  options:
    _url:
      description: A tiddlywiki url.
      type: string
      required: true
    auth:
      description: A tiddlywiki base auth list of [username, password]. This can be provided in the url
      type: list
      required: false
    cert_verify:
      description: A flag to control weather the serivce cert will be vaidated
      type: bool
      required: false
EXAMPLES: |
  {{ [tiddler_id] | munnox.bootstrap.tw_get_ids(tw_url, auth=('username', 'password'), cert_verify=false)}}
  {{ [tiddler_filter] | munnox.bootstrap.tw_filter(tw_url)}}
  {{ ['[all[tiddlers]!is[system]sort[title]]'] | munnox.bootstrap.tw_filter(tw_url)}}
RETURN:
  _value:
    description: The chosen tiddlywiki tiddler.
    type: raw
"""

@pass_environment
def munnox_tiddlywiki_interface_id(
    environment, tiddler_ids, tw_url, *args, **kw
):
    """Retrieve Tiddlywiki tiddlers using the server API."""
    keywords = dict(kw)
    timeout_sync = 30
    if 'timeout_sync' in keywords:
        timeout_sync = keywords.pop('timeout_sync')
    auth = None
    if 'auth' in keywords:
        auth = keywords.pop('auth')
    cert_verify = True
    if 'cert_verify' in keywords:
        cert_verify = bool(keywords.pop('cert_verify'))
    # display.warning(f"{auth}")
    tiddlers = []
    for tiddler_id in tiddler_ids:
        try:
            tw_client = tiddlywiki.twclient.TWClient(tw_url, verify=cert_verify, auth=tuple(auth))
            tiddler = tw_client.get(tiddler_id)
            if tiddler is None:
              tiddler_obj = None
            else:
              tiddler_obj = tiddler.to_dict()
            # {
            #     "item": tiddler_id,
            #     "tiddler": tiddler.to_dict(),
            #     "state": "OK"
            # }
        

        except AnsibleFilterError:
            raise
        # except tiddlywiki.errors.TWError as e:
        #     msg_error = f"tiddlywiki filter failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_id: {tiddler_id}",
        #     tiddler_obj = {
        #         "item": tiddler_id,
        #         "tiddler": None,
        #         "state": "FAILED",
        #         "error": msg_error
        #     }
        except Exception as e:
            msg_error = f"tiddlywiki filter failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_id: {tiddler_id}",
            raise AnsibleFilterError(
                msg_error,
                orig_exc=e
            )

        if tiddler_obj is not None:
          tiddlers.append(tiddler_obj)
    # results = [
    #     {
    #         "tw_url": tw_url,
    #         "tiddlers": tiddlers
    #     }
    # ]
    # results[0]['debug'] = {
    #     "tw_url": tw_url,
    #     "args": args,
    #     "kw": kw,
    # }

    return tiddlers

def munnox_tiddlywiki_interface_search(
    tiddler_filters, tw_url, *args, **kw
):
    """Retrieve Tiddlywiki tiddlers using the server API."""
    keywords = dict(kw)
    timeout_sync = 30
    if 'timeout_sync' in keywords:
        timeout_sync = keywords.pop('timeout_sync')
    auth = None
    if 'auth' in keywords:
        auth = keywords.pop('auth')
    cert_verify = True
    if 'cert_verify' in keywords:
        cert_verify = bool(keywords.pop('cert_verify'))
    if tiddler_filters is None or tiddler_filters == "":
        raise AnsibleFilterError(
            f"Tiddlywiki filter failed no tiddler_filter given the default can be ['[all[tiddlers]!is[system]sort[title]]'].",
        )

    tiddlers = []
    for tiddler_filter in tiddler_filters:
        try:
            tw_client = tiddlywiki.twclient.TWClient(tw_url, verify=cert_verify, auth=tuple(auth))
            tiddlers_found = tw_client.search(tiddler_filter)
            tiddler_obj = {
                "item": tiddler_filter,
                "tiddler": [ tiddler.to_dict() for tiddler in tiddlers_found ],
                "state": "ok"
            }
        except AnsibleFilterError:
            raise
        except tiddlywiki.errors.TWError as e:
            msg_error = f"tiddlywiki filter failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_filter: {tiddler_filter}",
            tiddler_obj = {
                "item": tiddler_filter,
                "tiddler": None,
                "state": "failed",
                "error": msg_error
            }
        except Exception as e:
            msg_error = f"tiddlywiki filter failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_filter: {tiddler_filter}",
            raise AnsibleFilterError(
                msg_error,
                orig_exc=e
            )
        tiddlers.append(tiddler_obj)
    # results = [
    #     {
    #         "tw_url": tw_url,
    #         "tiddlers": tiddlers
    #     }
    # ]
    # results[0]['debug'] = {
    #     "tw_url": tw_url,
    #     "args": args,
    #     "kw": kw,
    # }
    # results = tiddlers

    return tiddlers

class FilterModule(object):
    """Ansible core jinja2 filters"""

    def filters(self):
        return {
            "tw_get_ids": munnox_tiddlywiki_interface_id,
            "tw_search": munnox_tiddlywiki_interface_search,
            # debug
            "type_debug": lambda o: o.__class__.__name__,
        }
