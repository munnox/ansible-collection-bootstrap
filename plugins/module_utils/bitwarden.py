import base64
import os
import traceback
import subprocess
import json

# from ansible.utils.display import Display
# display = Display()
# display = print


# from https://github.com/ansible-collections/kubernetes.core/blob/main/plugins/modules/helm_repository.py
YAML_IMP_ERR = None
try:
    import yaml

    HAS_YAML = True
except ImportError:
    YAML_IMP_ERR = traceback.format_exc()
    HAS_YAML = False


class Bitwarden_CLI:

    @staticmethod
    def bw_clean_env(env):
        env = {k: env[k] for k in env if k in [
            "HOME", # required so the crediential file knows the home directory
            # "USER",
            "PATH", # Required to fing the BW binary
            "BW_USERNAME", # Required for the BW username
            "BW_PASSWORD", # Required for the BW password
            "BW_CLIENTID", # BW API Client ID
            "BW_CLIENTSECRET", # BW API Client Secret
            "BITWARDENCLI_APPDATA_DIR", # BW Path to place app data willl default to the home folder but can be overriden by this
            "BW_SESSION" # preserve a session id if given
        ]}
        return env

    @staticmethod
    def bw_login_api(client_id, client_secret, password=None, timeout=10, env=None):
        if isinstance(env, type(None)):
            env = {**os.environ}
            env = Bitwarden_CLI.bw_clean_env(env)

        # raise Exception(f"Env: {env}")
        # display.v(f"env_clean before:\n{env}")

        msg_error = []
        if ((client_id is None) or (client_id == "")) and ("BW_CLIENTID" not in env):
            msg_error.append(f"Bitwarden ID Not Given. Given='{client_id}' or set in Environment BW_CLIENTID")
        if ((client_secret is None) or (client_secret == "")) and ("BW_CLIENTSECRET" not in env):
            msg_error.append(f"\nBitwarden SECRET Not Given. Given='{client_id}' or set in Environment BW_CLIENTSECRET")
        if ((password is None) or (password == "")) and 'BW_PASSWORD' not in env:
            msg_error.append(f"\nBitwarden PASSWORD Not Given. Given='{password}' or set in Environment BW_PASSWORD")
        if len(msg_error) > 0:
            # display.error(f"{'\n'.join(msg_error)}")
            raise Exception('\n'.join(msg_error))
        

        # if "BW_CLIENTID" not in env:
        if client_id is not None:
            env["BW_CLIENTID"] = client_id
        # if "BW_CLIENTSECRET" not in env:
        if client_secret is not None:
            env["BW_CLIENTSECRET"] = client_secret
        # if "BW_PASSWORD" not in env:
        if password is not None:
            env["BW_PASSWORD"] = password
        
        # display.warning(f"env after:\n{env}")

        result = subprocess.run(
            ["bw", "login", "--apikey"],
            capture_output=True,
            timeout=timeout,
            env=env,
        )

        stdout = result.stdout.decode('utf-8')
        stderr = result.stderr.decode('utf-8')

        # display.vvv(f"bw login result ({result.returncode}):\nSTDOUT:\n'{stdout}'\nSTDERR:\n'{stderr}'")


        test_logged_in = "You are logged in!" in stdout
        test_result_returncode = result.returncode == 0
        test_already_logged_in = "You are already logged in" in stderr
        test_not_logged_in = "You are not logged in." in stderr
        assert (
            (not test_result_returncode and test_already_logged_in)
            or (test_result_returncode and test_logged_in)
        ), f"Cannot Login to vault {result}"


        session_id = Bitwarden_CLI.bw_unlock(password)[0]['session_id']
        # session_id = stdout # + f"{env}"
        return {"session_id": session_id}

    @staticmethod
    def bw_unlock(password, timeout=10, env=None, passenvname="BW_PASSWORD"):
        if isinstance(env, type(None)):
            env = {**os.environ}
            env = Bitwarden_CLI.bw_clean_env(env)
        if passenvname not in env:
            env[passenvname] = password

        result = subprocess.run(
            ["bw", "unlock", "--passwordenv", passenvname, "--raw"],
            capture_output=True,
            timeout=timeout,
            env=env,
        )

        assert result.returncode == 0, f"Cannot Unlock vault {result}"
        session_id = result.stdout
        return [{"session_id": str(session_id, "utf-8") }]

    @staticmethod
    def bw_sync(session_id, timeout=30, env=None):
        if isinstance(env, None):
            env = {}
        result = subprocess.run(
            ["bw", "sync", "--session", session_id],
            capture_output=True,
            timeout=timeout,
            env={**os.environ, **env},
        )
        assert (
            result.returncode == 0 and b"Syncing complete." in result.stdout
        ), "Cannot Sync vault"

    @staticmethod
    def bw_get_template(session_id, template, timeout=10, env=None, passenvname="BW_PASSWORD"):
        if isinstance(env, type(None)):
            env = {**os.environ}
            env = Bitwarden_CLI.bw_clean_env(env)

        valid_items = [
            "item",
            "item.field","item.login","item.login.uri","item.card","item.identity","item.securenote",
            "folder",
            "collection"
            "item-collections",
            "org-collection"
        ]
        
        if template not in valid_items:
            raise Exception(
                f"Bitwarden get template command failed - template item '{template}' Not found in: {valid_items}"
            )

        # https://bitwarden.com/help/cli/#get
        result = subprocess.run(
            ["bw", "get", "template", template, "--session", session_id],
            capture_output=True,
            timeout=timeout,
            env=env,
        )

        return Bitwarden_CLI.process_result(result)

    @staticmethod
    def bw_get_attachment(session_id, record_id, filename, output_path, timeout=10):
        # display.v(f"Bitwarden Get Attachment running")
        result = subprocess.run(
            [
                # bw get attachment <filename> --itemid <id>
                "bw",
                "get",
                "attachment", filename,
                "--itemid", record_id,
                "--session", session_id,
                "--output", output_path,
            ],
            capture_output=True,
            timeout=timeout,
        )
        assert result.returncode == 0, "Cannot get file attachment from vault"

    @staticmethod
    def bw_delete_attachment(session_id, record_id, filename, output_path, timeout=10):
        # display.v(f"Bitwarden Get Attachment running")
        result = subprocess.run(
            [
                # bw get attachment <filename> --itemid <id>
                "bw",
                "delete",
                "attachment", filename,
                "--itemid", record_id,
                "--session", session_id,
            ],
            capture_output=True,
            timeout=timeout,
        )
        assert result.returncode == 0, "Cannot get file attachment from vault"

    @staticmethod
    def bw_get_record(session_id, record_id):
        result = subprocess.run(
            [
                "bw", "get",
                "item", record_id,
                "--session", session_id
            ],
            capture_output=True,
            timeout=10,
        )
        # records = [Bitwarden_CLI.process_result(result)]
        return Bitwarden_CLI.process_result(result)

    @staticmethod
    def bw_list_records(session_id, search_term=None, full=False):
        args = [
            "bw", "list",
            "items",
        ]
        
        if search_term is not None and isinstance(search_term, str) and search_term != "":
            args += ["--search", search_term]

        args += [
            "--session", session_id
        ]
        print(args)
        result = subprocess.run(
            args,
            capture_output=True,
            timeout=30,
        )
        records = Bitwarden_CLI.process_result(result)
        if not full:
            records = [
                {
                    "id": rec["id"],
                    "name": rec["name"],
                    "notes": rec.get("notes", None),
                }
                for rec in records
            ]
        return records

    @staticmethod
    def process_result(result):
        stdout = result.stdout
        if result.returncode == 0:
            records = json.loads(stdout)

        elif result.returncode == 1 and (b"Lock file is already being held"):
            records = json.loads(stdout)
            # raise Warning(
            #     f"Bitwarden filter command Had a known Error based on 2023.12 https://github.com/bitwarden/clients/issues/7126 version CMD: '{' '.join(result.args)}'"
            # )

        elif result.returncode == 1 and (b"You are not logged in." in result.stderr):
            raise Exception(
                f"Bitwarden filter command failed - Not logged in: {' '.join(result.args)}"
            )
        elif result.returncode == 1 and (b"Not found." in result.stderr):
            raise Exception(
                f"Bitwarden filter command failed - ID Not found: '{' '.join(result.args)}'"
            )

        else:
            raise Exception(
                f"Bitwarden filter command failed unknown - cmd: {' '.join(result.args)}, "
                f"result.rc: {result.returncode}, "
                f"result.stderr: {result.stderr}, "
                # f"result.stdout first 200/{len(result.stdout)}: {result.stdout[:200]}"
            )
        return records

    # https://bitwarden.com/help/cli/#create
    # bw get template item |
    # jq ".name=\"Test Item\" | .login=$(bw get template item.login | jq '.username="me@example.com" | .password="password" | .uris=[{"match": null,"uri": "http://example.com/"}]')" |
    # bw encode |
    # bw create item
    # Login .type=1 item.login
    # Secure Note .type=2 item.note
    # Card .type=3 item.card
    # Identity .type=4 item.identity

    @staticmethod
    def bw_create_record(session_id, item_name, item_obj):
        valid_items = [
            "item",
            "attachment",
            "folder",
            "org-collection"
        ]
        if item_name not in valid_items:
            raise Exception(
                f"Bitwarden create command failed - create item '{item_name}' Not found in: {valid_items}"
            )

        encoded = base64.b64encode(bytes(json.dumps(item_obj), "utf-8"))

        encode_str = encoded.decode("utf-8")

        creation_result = None
        try:
            result_creation = subprocess.run(
                [
                    "bw",
                    "create",
                    "item",
                    f"'{encode_str}'",
                    #   , record_id,
                    "--session", session_id,
                ],
                capture_output=True,
                timeout=10,
            )

            creation_result = json.loads(result_creation.stdout)
        except Exception as e:
            raise Exception(
                f"Bitwarden create command failed - Cannot parse bw cli stdout. Item: {item_obj}, stdout: {result_creation.stdout}"
            )

        return creation_result
