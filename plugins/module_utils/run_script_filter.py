
# Run a given script
## WARNING this allow arbitary scripts to be run
def run_script_filter(env, script, *args, **kwargs):

    compiled_script = compile(
        script,
        'filter_script_arg',
        'exec'
    ) 
    f = exec(compiled_script, env)
    print(f"return of exec:\n{f}\n{env.keys()}")
    result = env['process'](*args, **kwargs)
    return result
