# python 3 headers, required if submitting to Ansible
# Dervied from https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html#developing-lookup-plugins

# from __future__ import (absolute_import, division, print_function)
# __metaclass__ = type

DOCUMENTATION = r"""
  name: tiddlywiki_get_ids
  author: Robert Munnoch
  version_added: "1.0.0"  # for collections, use the collection version, not the Ansible version
  short_description: Get Tiddlers from a tiddlywiki node.js service using web API
  description:
      - This lookup returns the tiddlers given by id (title) from a tiddlywiki node.js service given an auth
  options:
    _terms:
      description:
        - Tiddler ids (title(s)) to get.
      required: True
    ids:
      description: A List of Tiddler IDs
      type: list
      default: []
    url:
      description: The URL of the Tiddlywiki service
      type: string
    auth:
      description: Basic Auth tuple [username, password]
      type: list
    method:
      description: (WIP) A reserved keyword to allow the lookup to do owth things in the future
      type: string
      default: get
    cert_verify:
      description: A flag to control whether the serivce cert will be vaidated
      type: bool
      required: false
  notes:
    - This is a simplification to able to get tiddlers with a list with a given url and auth
"""

RETURN = """
_raw:
  description:
    - a tiddler, item item
  type: list
  elements: dict
"""

from ansible.errors import AnsibleError, AnsibleParserError
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
import urllib3
from ansible_collections.munnox.bootstrap.plugins.module_utils.flatten_list import flatten_list
from ansible.module_utils._text import to_native

# Futher info here https://gitlab.com/munnox/python-tiddlywiki
import tiddlywiki

display = Display()

class LookupModule(LookupBase):

    def run(self, terms, variables, **kwargs):
      

      # NOTE: Variable seem to hole the whole hostvars and run variables
      # display.warning(f"Variables: {variables}\n\n\nkwargs: {kwargs}")

      # # First of all populate options,
      # # this will already take into account env vars and ini config
      self.set_options(var_options=variables, direct=kwargs)

      if not terms:
        terms = self.get_option('ids')
      
      if isinstance(terms[0], list):
         terms = flatten_list(terms)
      
      tiddler_ids = terms

      tw_url = self.get_option('url')
      auth = self.get_option('auth')
      cert_verify = self.get_option('cert_verify')
      method = self.get_option('method')

      display.v(f"record_ids: '{tiddler_ids}', auth: '{auth}', certificate verification: '{cert_verify}, method: {method}")

      secure_warning = True
      if secure_warning:
        urllib3.disable_warnings()
      # lookups in general are expected to both take a list as input and output a list
      # this is done so they work with the looping construct 'with_'.
      tiddlers = []
      for tiddler_id in tiddler_ids:
          try:
              tw_client = tiddlywiki.twclient.TWClient(tw_url, verify=False, auth=tuple(auth))
              tiddler = tw_client.get(tiddler_id)
              if tiddler is None:
                tiddler_obj = {
                    "item": tiddler_id,
                    "failed": False,
                    "missing": True,
                }
              else:
                tiddler_obj = {
                    "item": tiddler_id,
                    "tiddler": tiddler.to_dict(),
                    "failed": False,
                    "missing": False,
                }

          

          except tiddlywiki.errors.TWError as e:
              msg_error = f"tiddlywiki lookup failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_id: {tiddler_id}",
              tiddler_obj = {
                  "item": tiddler_id,
                  "tiddler": None,
                  "failed": True,
                  "error": msg_error
              }
          except Exception as e:
              msg_error = f"tiddlywiki lookup failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_id: {tiddler_id}",
              raise AnsibleError(msg_error,orig_exc=e)
          tiddlers.append(tiddler_obj)
      
      # for term in terms:

          # Find the file in the expected search path, using a class method
          # that implements the 'expected' search path for Ansible plugins.
          # lookupfile = self.find_file_in_search_path(variables, 'files', term)

          # Don't use print or your own logging, the display class
          # takes care of it in a unified way.
          # display.vvvv(u"File lookup using %s as file" % lookupfile)
          # try:
          #     if lookupfile:
          #         contents, show_data = self._loader._get_file_contents(lookupfile)
          #         ret.append(contents.rstrip())
          #     else:
          #         # Always use ansible error classes to throw 'final' exceptions,
          #         # so the Ansible engine will know how to deal with them.
          #         # The Parser error indicates invalid options passed
          #         raise AnsibleParserError()
          # except AnsibleParserError:
          #     raise AnsibleError("could not locate file in lookup: %s" % term)

          # consume an option: if this did something useful, you can retrieve the option value here
          # if self.get_option('option1') == 'do something':
          #   pass
      
      # display.debug(f"Tiddlers: {tiddlers}")
      return tiddlers