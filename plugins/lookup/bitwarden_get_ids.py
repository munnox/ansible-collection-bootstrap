# python 3 headers, required if submitting to Ansible
# Dervied from https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html#developing-lookup-plugins

# from __future__ import (absolute_import, division, print_function)
# __metaclass__ = type

DOCUMENTATION = r"""
  name: bitwarden_get_ids
  author: Robert Munnoch
  version_added: "1.0.0"  # for collections, use the collection version, not the Ansible version
  short_description: Get Secrets from Bitwarden using terminal bw CLI
  description:
      - This lookup returns the secret given by id from a BW CLI program given a session id.
  options:
    _terms:
      description:
        - Secret Record ID to get.
      required: True
    ids:
      description: Record IDs
      type: list
      default: []
    session_id:
      description: Session ID
      type: string
  notes:
    - This Bitwarden interface using the BW CLI and uses a suprocess to run the program.
    - This is a simplification to able to get secret with a given session id
"""

RETURN = """
_raw:
  description:
    - a record
  type: list
  elements: dict
"""

import os
from ansible.errors import AnsibleError, AnsibleParserError
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
from ansible_collections.munnox.bootstrap.plugins.module_utils.bitwarden import (
    Bitwarden_CLI,
)
from ansible_collections.munnox.bootstrap.plugins.module_utils.flatten_list import flatten_list

display = Display()

class LookupModule(LookupBase):

    def run(self, terms, variables, **kwargs):
      
      # # First of all populate options,
      # # this will already take into account env vars and ini config
      self.set_options(var_options=variables, direct=kwargs)

      if not terms:
        terms = self.get_option('ids')
      
      if isinstance(terms[0], list):
         terms = flatten_list(terms)

      record_ids = terms

      session_id = self.get_option('session_id')

      if session_id is None:
        session_id = os.environ.get('BW_SESSION', None)
      
      if session_id is None:
        raise AnsibleParserError("Bitwarden Lookup has no Session ID Given. Use 'session_id' to set one at runtime or BW_SESSION environment variable")  

      display.v(f"record ids: '{record_ids}' Session id: '{session_id}'")

      # lookups in general are expected to both take a list as input and output a list
      # this is done so they work with the looping construct 'with_'.
      records = []
      for record_id in record_ids:
          display.debug("Bitwarden lookup record_id: %s" % record_id)
          try:
              record = Bitwarden_CLI.bw_get_record(
                  session_id=session_id, record_id=record_id
              )
              display.debug("Bitwarden lookup record_id result: %s" % record)
              records.append({
                  "item": record_id,
                  "record": record
              })
          # except Warning as error:
          #     display.warning(error)
          except Exception as error:
              raise AnsibleError(f"General Bitwarden Lookup Exception raised error: {error}. Given record_ids: {record_ids}")

      # for term in terms:

          # Find the file in the expected search path, using a class method
          # that implements the 'expected' search path for Ansible plugins.
          # lookupfile = self.find_file_in_search_path(variables, 'files', term)

          # Don't use print or your own logging, the display class
          # takes care of it in a unified way.
          # display.vvvv(u"File lookup using %s as file" % lookupfile)
          # try:
          #     if lookupfile:
          #         contents, show_data = self._loader._get_file_contents(lookupfile)
          #         ret.append(contents.rstrip())
          #     else:
          #         # Always use ansible error classes to throw 'final' exceptions,
          #         # so the Ansible engine will know how to deal with them.
          #         # The Parser error indicates invalid options passed
          #         raise AnsibleParserError()
          # except AnsibleParserError:
          #     raise AnsibleError("could not locate file in lookup: %s" % term)

          # consume an option: if this did something useful, you can retrieve the option value here
          # if self.get_option('option1') == 'do something':
          #   pass
      
      # display.debug(f"Records: {records}")
      return records