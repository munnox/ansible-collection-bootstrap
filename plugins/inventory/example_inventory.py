# Copyright (c) 2024 Robert Munnoch
# Using the documentation https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html#inventory-object
# To create a minimal inventory plugin to understand and extend it better in the future
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function

from ansible.errors import AnsibleError, AnsibleParserError
from collections.abc import Mapping
from ansible import constants as C

__metaclass__ = type

DOCUMENTATION = """
    name: example_inventory
    author:
        - Robert Munnoch
    description:
        - Example Inventory Plugin for a minimal example of a useable inventory with construction capabilities
    extends_documentation_fragment:
        - constructed
        - inventory_cache
    options:
        plugin:
            description: token that ensures this is a source file for the 'example_inventory' plugin.
            required: True
            choices: ['munnox.bootstrap.example_inventory']
        var1:
            description: var1
            required: True
            env:
                - name: VAR1
        var2:
            description: var2
            required: True
            env:
                - name: VAR2
"""

EXAMPLES = """
# 00_example_inventory.yml file in YAML format
# Example command line: ansible-inventory -v --list -i 00_example_inventory.yml

plugin: munnox.bootstrap.example_inventory
var1: testing variable 1
var2: testing variable 2

# This will define two groups grp1,grp2 and three hosts host1,host2,host3 and add the variables to host1
"""

from ansible.constants import DEFAULT_LOCAL_TMP
from ansible.plugins.inventory import BaseInventoryPlugin, Constructable, Cacheable
from ansible.module_utils._text import to_text, to_native

class InventoryModule(BaseInventoryPlugin, Constructable, Cacheable):
    NAME = "munnox.bootstrap.example_inventory"

    # https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html#inventory-object
    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path)
        # Parse the plugin config file
        config = self.loader.load_from_file(path, cache=False)
        self.set_options(direct=config, var_options=self._vars)

        self.load_cache_plugin()
        print(f"print inventory path: {path}")
        print(f"default temp path: {DEFAULT_LOCAL_TMP}")


        # Useful properties https://github.com/ansible/ansible/blob/devel/lib/ansible/inventory/data.py
        print(f"print inventory hosts: {inventory.hosts}")
        print(f"print inventory groups: {inventory.groups}")
        print(f"print inventory localhost: {inventory.localhost}")
        print(f"print inventory current source: {inventory.current_source}")
        print(f"print inventory processed sources: {inventory.processed_sources}")

        self.variable1 = self.get_option("var1")
        if self.variable1 is None:
            raise AnsibleParserError("No varaible1 defined plugin: 'var1: testing' to the yamp plugin or refer to the documentation. ")
        self.variable2 = self.get_option("var2")
        if self.variable2 is None:
            raise AnsibleParserError("No varaible1 defined plugin: 'var2: testing' to the yamp plugin or refer to the documentation. ")

        self.inventory.add_group("grp1")
        self.inventory.add_group("grp2")
        self.inventory.add_child("grp1", "grp2")

        self.inventory.add_host(
            host="host1",
            group="grp1",
        )

        self.inventory.add_host(
            host="host2",
            group="grp2",
        )
        self.inventory.add_host(
            host="host3",
        )

        self.inventory.set_variable("host1", "test_var1", self.variable1)
        self.inventory.set_variable("host1", "test_var2", self.variable2)