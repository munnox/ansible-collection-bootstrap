# Copyright (c) 2024 Robert Munnoch GPLv3
# Using the documentation https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html#inventory-object
# Tiddlywiki API info from https://gitea.munnoxtech.com/munnox/python-tiddlywiki/src/branch/main/tiddlywiki/twclient.py
# To create a Tiddlywiki Inventory to be able to use the Tiddlywiki UI to modify and tweak the ansible inventory.

from __future__ import absolute_import, division, print_function
import json
import re

from ansible.errors import AnsibleError, AnsibleParserError
from collections.abc import Mapping
from ansible import constants as C

__metaclass__ = type

DOCUMENTATION = """
    name: tiddlywiki_inventory
    author:
        - Robert Munnoch
    description:
        - Inventory Plugin for a useable inventory stored as tiddlywiki tiddlers with construction capabilities
    extends_documentation_fragment:
        - constructed
        - inventory_cache
    options:
        plugin:
            description: token that ensures this is a source file for the 'tiddlywiki_inventory' plugin.
            required: True
            choices: ['munnox.bootstrap.tiddlywiki_inventory']
        url:
            description: url
            required: True
            env:
                - name: https://tw.example.com/
        username:
            description: username
            required: False
            env:
                - name: user
        password:
            description: password
            required: False
            env:
                - name: pass
        tiddler:
            description:
                - Inventory Tiddler this contains the meta data to know where the rest of the inventory is to be found in the tiddlywiki.
                - The assumption currently is that there can be mulitple inventories existing side by side in the wiki. and this is the Hypergraph organising element.
            default: Inventory
"""

EXAMPLES = """
# 00_example_inventory.yml file in YAML format
# Example command line: ansible-inventory -v --list -i 00_example_inventory.yml

plugin: munnox.bootstrap.example_inventory
url: https://tw.example.com/
username: user
password: pass
tiddler: Inventory

# This will try to log in to a tiddlywiki and auth via the username and password given.
"""

from ansible.constants import DEFAULT_LOCAL_TMP
from ansible.plugins.inventory import BaseInventoryPlugin, Constructable, Cacheable
from ansible.module_utils._text import to_text, to_native
# https://github.com/ansible/ansible/blob/devel/lib/ansible/module_utils/urls.py
from ansible.module_utils.urls import open_url, basic_auth_header
from ansible.module_utils.six.moves.urllib import error as urllib_error
from ansible.module_utils.six.moves.urllib.parse import urlencode, quote, urljoin

class InventoryModule(BaseInventoryPlugin, Constructable, Cacheable):
    NAME = "munnox.bootstrap.tiddlywiki_inventory"

    def get_tiddler(self, tiddler):
        # API info from https://gitea.munnoxtech.com/munnox/python-tiddlywiki/src/branch/main/tiddlywiki/twclient.py
        result = self.get_info(f"{self.url}/recipes/default/tiddlers/{tiddler}", self.username, self.password)
        return result

    def search_tiddler(self, filter: str = None, exclude: str = None, **kwargs):
        # API info from https://gitea.munnoxtech.com/munnox/python-tiddlywiki/src/branch/main/tiddlywiki/twclient.py
         # If the filter is None then use the default search filter.
        params = {"filter": filter}
        if filter is None:
            params["filter"] = "[all[tiddlers]!is[system]sort[title]]"
        if exclude is not None:
            params["exclude"] = exclude
        quoted_params = urlencode(
            params, safe="()", quote_via=quote
        )  # quote(search_filter)       quoted_params = para
        result = self.get_info(f"{self.url}/recipes/default/tiddlers.json?{quoted_params}", self.username, self.password)
        return result

    # Code lifted from https://gitea.munnoxtech.com/munnox/python-tiddlywiki/src/branch/main/tiddlywiki/twclient.py
    @staticmethod
    def process_tag_string_to_list(tag_string: str):
        """Convert a tiddlywiki tag string to a list of tags."""
        tagpattern = re.compile(
            r"(?P<grp>(?P<tag>[\w\/]+)|\[\[(?P<tags>[\w \/]+)\]\]) *"
        )
        matches = tagpattern.finditer(tag_string)
        tags = []
        for match in matches:
            groupdict = match.groupdict()
            tag = groupdict["tag"]
            tagspace = groupdict["tags"]
            final_tag = tagspace if tagspace is not None else tag
            tags.append(final_tag)
        return tags

    def get_info(self, url, username, password):
        
        # Absracted result varaible ready for futher config
        self.timeout = 1
        self.validate_certs = False

        self.headers = {}
        self.headers.update({"Authorization": f"{basic_auth_header(username, password)}" })

        self.display.v(f"Fetching: {url}, Headers: {self.headers}")
        try:
            # https://github.com/ansible/ansible/blob/devel/lib/ansible/module_utils/urls.py
            response = open_url(
                url,
                headers=self.headers,
                timeout=self.timeout,
                validate_certs=self.validate_certs
                # follow_redirects=self.follow_redirects,
                # client_cert=self.cert,
                # client_key=self.key,
                # ca_path=self.ca_path,
            )
        except urllib_error.HTTPError as e:
            """Return an exception with further infomration to help debugging."""
            if e.code == 403:
                self.display.display( f"Permission denied: {url}.", color="red")

            raise AnsibleError(to_native(e.fp.read()) + f"{e}")

        try:
            raw_data = to_text(response.read(), errors="surrogate_or_strict")
        except UnicodeError:
            raise AnsibleError(
                "Incorrect encoding of fetched payload from Tiddlywiki API."
            )

        try:
            results = json.loads(raw_data)
        except ValueError:
            raise AnsibleError("Incorrect JSON payload: %s" % raw_data)
        
        return results

    # https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html#inventory-object
    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path)
        # Parse the plugin config file
        config = self.loader.load_from_file(path, cache=False)
        self.set_options(direct=config, var_options=self._vars)

        self.load_cache_plugin()
        print(f"print inventory path: {path}")
        print(f"default temp path: {DEFAULT_LOCAL_TMP}")


        # Useful properties https://github.com/ansible/ansible/blob/devel/lib/ansible/inventory/data.py
        print(f"print inventory hosts: {inventory.hosts}")
        print(f"print inventory groups: {inventory.groups}")
        print(f"print inventory localhost: {inventory.localhost}")
        print(f"print inventory current source: {inventory.current_source}")
        print(f"print inventory processed sources: {inventory.processed_sources}")

        self.url = self.get_option("url")
        if self.url is None:
            raise AnsibleParserError("No url defined. plugin: 'url: https://tw.munnox.com' to the yamp plugin or refer to the documentation. ")
        self.username = self.get_option("username")
        if self.username is None:
            raise AnsibleParserError("No username defined. plugin: 'username: user' to the yamp plugin or refer to the documentation. ")
        self.password = self.get_option("password")
        if self.password is None:
            raise AnsibleParserError("No password defined. plugin: 'password: user' to the yamp plugin or refer to the documentation. ")
        self.inventory_tiddler = self.get_option("tiddler")
        if self.password is None:
            raise AnsibleParserError("No Tiddler defined. plugin: 'tiddler: inventory' to the yamp plugin or refer to the documentation. ")

        inventory_tiddler = self.get_tiddler(self.inventory_tiddler)
        inventory_tag = inventory_tiddler['fields']['inventory_tag']
        host_tag = inventory_tiddler['fields']['host_tag']
        group_tag = inventory_tiddler['fields']['group_tag']
        print(f"inventory_tiddler: {inventory_tiddler}")
        print(f"inventory_tags: {inventory_tag}")
        print(f"host_tags: {host_tag}")
        print(f"group_tags: {group_tag}")

        all_tiddlers = self.search_tiddler()
        # print(f"all_tiddlers: {all_tiddlers}")

        tagged_tiddlers = []
        groups = {}
        hosts = {}
        for tid in all_tiddlers:
            # new_tid = {
            #     "title": tid['title'],
            #     "tags": InventoryModule.process_tag_string_to_list(tid.get('tags', ''))
            # }
            tags = InventoryModule.process_tag_string_to_list(tid.get('tags', ''))
            if inventory_tag in tags:
                full_tid = self.get_tiddler(tid['title'])
                full_tid['tags'] = InventoryModule.process_tag_string_to_list(full_tid['tags'])
                if group_tag in tags:
                    groups[tid['title']] = tags
                    self.inventory.add_group(tid['title'])
                if host_tag in tags:
                    hosts[tid['title']] = tags
                    self.inventory.add_host(tid['title'])
                    # self.inventory.add_host(tid['title'], group="grp_tiddlywiki")
                # tagged_tiddlers.append([tid])

        for host in hosts:
            grps = []
            for tag in hosts[host]:
                if tag in groups.keys():
                    grps.append(tag)
            for grp in grps:
                self.inventory.add_host(host, group=grp)
        
        print(f"tagged_tiddlers: {tagged_tiddlers}")
        print(f"groups: {groups}")
        print(f"hosts: {hosts}")

        self.inventory.add_group("grp_tiddlywiki")
        # self.inventory.add_group("grp2")
        # self.inventory.add_child("grp1", "grp2")

        # self.inventory.add_host(
        #     host="sia",
        #     group="grp1",
        # )

        # self.inventory.add_host(
        #     host="host2",
        #     group="grp2",
        # )
        self.inventory.add_host(
            host="inventory_tiddler",
            group="grp_tiddlywiki"
        )

        self.inventory.set_variable("grp_tiddlywiki", "url", self.url)
        self.inventory.set_variable("inventory_tiddler", "url", self.url)
        self.inventory.set_variable("inventory_tiddler", "username", self.username)
        self.inventory.set_variable("inventory_tiddler", "inventory", inventory_tiddler)

        self.inventory.reconcile_inventory()