# Derived from https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html#callback-plugins
# Make coding more python3-ish, this is required for contributions to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

# not only visible to ansible-doc, it also 'declares' the options the plugin requires and how to configure them.
DOCUMENTATION = '''
name: include_marker
callback_type: aggregate
requirements:
    - enable in configuration
short_description: Adds time to play stats
version_added: "2.0"  # for collections, use the collection version, not the Ansible version
description:
    - This callback just adds a Stronger marker around includes
'''
from datetime import datetime

from ansible.plugins.callback import CallbackBase


class CallbackModule(CallbackBase):
    """
    This callback module tells you how long your plays ran for.
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = 'munnox.bootstrap.include_marker'

    # only needed if you ship it and don't want to enable by default
    CALLBACK_NEEDS_ENABLED = True

    def __init__(self):

      # make sure the expected objects are present, calling the base's __init__
      super(CallbackModule, self).__init__()

    # def v2_on_any(self, *args, **kwargs):
    #    self._display.display(f"On any event -> args: {repr(args)}, kwargs: {repr(kwargs)}")
   
    def v2_playbook_on_include(self, included_file):
        self._display.display(f"On playbook include -> included file: {included_file._filename}")
