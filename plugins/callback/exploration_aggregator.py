# Derived from https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html#callback-plugins
# And from https://github.com/ansible/ansible/blob/devel/lib/ansible/plugins/callback/__init__.py
# And from https://github.com/ansible/ansible/blob/devel/lib/ansible/plugins/callback/default.py

# Make coding more python3-ish, this is required for contributions to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

# not only visible to ansible-doc, it also 'declares' the options the plugin requires and how to configure them.
DOCUMENTATION = '''
name: exploration_aggregator
callback_type: aggregate
requirements:
    - enable in configuration
short_description: Explores the Callback interface and adds time to play stats
version_added: "2.0"  # for collections, use the collection version, not the Ansible version
description:
    - This callback explores the callback and act as a verbose plugin for further development.
    - Also adds total play duration to the play stats.
options:
  format_string:
    description: format of the string shown to user at play end
    ini:
      - section: callback_munnox_bootstrap_exploration_aggregator
        key: format_string
    env:
      - name: ANSIBLE_CALLBACK_MUNNOX_BOOTSTAP_EXPLORATION_AGGREGATOR_FORMAT_STRING
    default: "Playbook run took %s days, %s hours, %s minutes, %s seconds"
'''
from datetime import datetime

from ansible.plugins.callback import CallbackBase


class CallbackModule(CallbackBase):
    """
    This callback module tells you how long your plays ran for.
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = 'munnox.bootstrap.exploration_aggregator'

    # only needed if you ship it and don't want to enable by default
    CALLBACK_NEEDS_ENABLED = True

    def __init__(self):

      # make sure the expected objects are present, calling the base's __init__
      super(CallbackModule, self).__init__()

      # start the timer when the plugin is loaded, the first play should start a few milliseconds after.
      self.start_time = datetime.now()

    def _days_hours_minutes_seconds(self, runtime):
      ''' internal helper method for this callback '''
      minutes = (runtime.seconds // 60) % 60
      r_seconds = runtime.seconds - (minutes * 60)
      return runtime.days, runtime.seconds // 3600, minutes, r_seconds

    def v2_on_any(self, *args, **kwargs):
       self._display.display(f"On any event -> args: {repr(args)}, kwargs: {repr(kwargs)}")
   
    # Task Finish events
    def v2_runner_on_ok(self, result):
       self._display.display(f"On task finish OK, Result: {repr(dir(result))}")

    def v2_runner_on_failed(self, result, ignore_errors=False):
       self._display.display(f"On task finsh FAILED, ignore_error: {ignore_errors}, Result: {repr(result)}")

    def v2_runner_on_unreachable(self, result):
       self._display.display(f"On task finsh UNREACHABLE, Result: {repr(result)}")
    
    # On include file
    def v2_playbook_on_include(self, included_file):
        self._display.display(f"On playbook include -> included file: {repr(included_file._filename)} {', '.join([h.name for h in included_file._hosts])}")

    # this is only event we care about for display, when the play shows its summary stats; the rest are ignored by the base class
    def v2_playbook_on_stats(self, stats):
      end_time = datetime.now()
      runtime = end_time - self.start_time

      # Shows the usage of a config option declared in the DOCUMENTATION variable. Ansible will have set it when it loads the plugin.
      # Also note the use of the display object to print to screen. This is available to all callbacks, and you should use this over printing yourself
      self._display.display(
         "munnox_bootstrap.exploration_aggregator -> "
         + self._plugin_options['format_string'] % (
            self._days_hours_minutes_seconds(runtime)
          )
      )