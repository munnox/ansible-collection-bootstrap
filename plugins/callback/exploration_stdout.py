# Derived from https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html#callback-plugins
# And from https://github.com/ansible/ansible/blob/devel/lib/ansible/plugins/callback/__init__.py
# And from https://github.com/ansible/ansible/blob/devel/lib/ansible/plugins/callback/default.py

# Using doc fragment https://github.com/ansible/ansible/blob/devel/lib/ansible/plugins/doc_fragments/result_format_callback.py

# Make coding more python3-ish, this is required for contributions to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

# not only visible to ansible-doc, it also 'declares' the options the plugin requires and how to configure them.
DOCUMENTATION = '''
name: exploration_stdout
type: stdout
short_description: Explores the stdout Callback interface and adds time to play stats
version_added: "2.0"  # for collections, use the collection version, not the Ansible version
description:
    - This callback explores the callback and act as a verbose plugin for further development.
    - Also adds total play duration to the play stats.
extends_documentation_fragment:
    - result_format_callback
options:
  show_callback_trace:
    description: format of the string shown to user at play end
    ini:
      - section: callback_munnox_bootstrap_exploration_stdout
        key: show_callback_trace
    env:
      - name: ANSIBLE_CALLBACK_MUNNOX_BOOTSTAP_EXPLORATION_STDOUT_SHOW_CALLBACK_TRACE
    default: False
  format_string:
    description: format of the string shown to user at play end
    ini:
      - section: callback_munnox_bootstrap_exploration_stdout
        key: format_string
    env:
      - name: ANSIBLE_CALLBACK_MUNNOX_BOOTSTAP_EXPLORATION_STDOUT_FORMAT_STRING
    default: "Playbook run took %s days, %s hours, %s minutes, %s seconds"
requirements:
  - set as stdout in configuration
'''
from datetime import datetime

from ansible import constants as C
from ansible.plugins.callback import CallbackBase
from ansible.executor.task_result import TaskResult
from ansible.executor.stats import AggregateStats
from ansible.playbook.task import Task
from ansible.inventory.host import Host
from ansible.playbook.play import Play
from ansible.playbook.included_file import IncludedFile

class CallbackModule(CallbackBase):
    """
    This callback module tells you how long your plays ran for.
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'munnox.bootstrap.exploration_stdout'

    # # only needed if you ship it and don't want to enable by default
    # CALLBACK_NEEDS_ENABLED = True

    def __init__(self):

      # make sure the expected objects are present, calling the base's __init__
      super(CallbackModule, self).__init__()

      # start the timer when the plugin is loaded, the first play should start a few milliseconds after.
      self.start_time = datetime.now()


      # import traceback
      # for line in traceback.format_stack():
      #   print(line.strip())


    def _days_hours_minutes_seconds(self, runtime):
      ''' internal helper method for this callback '''
      minutes = (runtime.seconds // 60) % 60
      r_seconds = runtime.seconds - (minutes * 60)
      return runtime.days, runtime.seconds // 3600, minutes, r_seconds

    def v2_on_any(self, *args, **kwargs):
      # https://github.com/ansible/ansible/blob/devel/lib/ansible/playbook/task.py
      # 
      try:
          show_callback_trace = self.get_option('show_callback_trace')
      except KeyError:
          # Callback does not declare show_callback_trace nor extend show_callback_trace_callback
          show_callback_trace = False

      # arg_props_private = [s for s in dir(args[0]) if not s.startswith('__')]
      arg_props_public = {}
      for i,a in enumerate(args):
        l = [
          repr(type(a)),
          {
            'play': isinstance(a, Play),
            'host': isinstance(a, Host),
            'task': isinstance(a, Task),
            'taskresult': isinstance(a, TaskResult),
            'aggreatestats': isinstance(a, AggregateStats),
            'includefile': isinstance(a, IncludedFile),
          },
        ]
        for s in dir(a):
          if not s.startswith('__'):
            l.append(s)
            
        arg_props_public[i] = l

      # Investigate the callstack and understand where the events are being called
      if (show_callback_trace):
        import traceback
        for line in traceback.format_stack():
          self._display.display(line.strip())

      # public_vars = '\n'.join([repr(s) for s in arg_props_public])
      public_vars = self._dump_results(arg_props_public)

      # detect start play event
      if (len(args) == 1 and isinstance(args[0], Play)):
        play = args[0]
        # play.gather_facts = False
        self._display.banner(f"Start PLAY '{play.name}'")
        # self._print_task_path(task)

      # detect start task event
      if (len(args) == 1 and isinstance(args[0], Task) and 'is_conditional' in kwargs):
        task = args[0]
        self._display.banner(f"Start TASK '{task.name}' action={task.action} no_log={task.no_log} {list(task.args.keys())}")
        self._print_task_path(task)

      
      # arg_props_public = [s for s in dir(args[0]) if not s.startswith('_')]
      if (isinstance(args[0], Play)):
        play = args[0]
        public_vars = len(public_vars)
        self._display.display(f"On any event '{play.name}' play -> {len(args)} {repr([type(a) for a in args])} args public:\n{public_vars}\nkwargs: {repr(kwargs)}", color=C.COLOR_VERBOSE)
      elif (isinstance(args[0], Host)):
        host = args[0]
        public_vars = len(public_vars)
        self._display.display(f"On any event '{host.name}' '{host._uuid}' host -> {len(args)} {repr([type(a) for a in args])} args public:\n{public_vars}\nkwargs: {repr(kwargs)}", color=C.COLOR_VERBOSE)
      elif (isinstance(args[0], Task)):
        task = args[0]
        public_vars = len(public_vars)
        self._display.display(f"On any event '{task.name}' task -> {len(args)} {repr([type(a) for a in args])} args public:\n{public_vars}\nkwargs: {repr(kwargs)}", color=C.COLOR_VERBOSE)
      elif (isinstance(args[0], TaskResult)):
        result = args[0]
        host = result._host
        task = result._task
        check_key = result._check_key
        task_fields = result._task_fields
        is_changed = result.is_changed()
        is_failed = result.is_failed()
        is_skipped = result.is_skipped()
        is_unreachable = result.is_unreachable()
        result_str = self._dump_results(result._result)
        if task.name != "Gathering Facts":
          self._display.display(f"On any event '{result.task_name}' taskresult -> {len(args)}\n{self._dump_results(result._result)}\nkwargs: {repr(kwargs)}\n{len(result_str)}", color=C.COLOR_DEBUG)
        else:
          self._display.display(f"On any event '{result.task_name}' taskresult -> Suppressing Gathering Facts Task Result", color=C.COLOR_DEBUG)
      elif (isinstance(args[0], IncludedFile)):
        includefile = args[0]
        task = includefile._task
        public_vars = len(public_vars)
        self._display.display(f"On any event '{task.name}' includefile -> {len(args)} {repr([type(a) for a in args])} args public:\n{public_vars}", color=C.COLOR_VERBOSE)
      else:
        # public_vars = len(public_vars)
        self._display.display(f"On any event -> {len(args)} {repr([type(a) for a in args])} args public:\n{public_vars}\nkwargs: {repr(kwargs)}", color=C.COLOR_VERBOSE)

      # detect endevent
      if (len(args) == 1 and isinstance(args[0], TaskResult)):
        result = args[0]
        self._display.banner(f"End TASK '{result.task_name}'")
  
    def v2_runner_on_start(self, host: Host, task: Task):
      # assert host is str
      # assert task is str
      # self._display.display(f"=========start=========\n")
      self._display.display(f"On task Start -> host: {host.name}, task: {task.name}", color=C.COLOR_OK)

    # Task Finish events
    def v2_runner_on_ok(self, result):
      self.end_task("OK", result)
      #  self._display.display(f"On task finish OK -> Result: {repr(dir(result))} Task: {result.task_name}\n\n", color=C.COLOR_OK)

    def v2_runner_on_failed(self, result, ignore_errors=False):
      self.end_task("FAILED", result, ignore_errors=ignore_errors)
      #  self._display.display(f"On task finsh FAILED -> ignore_error: {ignore_errors}, Result: {repr(result)}\n\n", color=C.COLOR_ERROR)

    def v2_runner_on_unreachable(self, result):
      self.end_task("UNREACHABLE", result)
      #  self._display.display(f"On task finsh UNREACHABLE -> Result: {repr(result)}\n\n", color=C.COLOR_UNREACHABLE)
    
    def v2_runner_on_skipped(self, result):
      self.end_task("SKIPPED", result)
      #  self._display.display(f"On task finsh SKIPPED -> Result: {repr(result)}\n\n", color=C.COLOR_SKIP)

    def end_task(self, event, result, ignore_errors=False):
      if event == "SKIPPED":
        self._display.display(f"On task finsh SKIPPED -> Result: {repr(result)}\n", color=C.COLOR_SKIP)
      elif event == "UNREACHABLE":
        self._display.display(f"On task finsh UNREACHABLE -> Result: {repr(result)}\n", color=C.COLOR_UNREACHABLE)
      elif event == "FAILED":
        self._display.display(f"On task finsh FAILED -> ignore_error: {ignore_errors}, Result: {repr(result)}\n", color=C.COLOR_ERROR)
      elif event == "OK":
        self._display.display(f"On task finish OK -> Result: {repr(dir(result))} Task: {result.task_name}\n", color=C.COLOR_OK)
      # self._display.display(f"==========end========\n")

    # On include file
    def v2_playbook_on_include(self, included_file):
        self._display.display(f"On playbook include -> included file: {included_file._filename} hosts: {', '.join([h.name for h in included_file._hosts])}", color=C.COLOR_OK)

    def v2_playbook_on_stats(self, stats):
      end_time = datetime.now()
      runtime = end_time - self.start_time

      # Shows the usage of a config option declared in the DOCUMENTATION variable. Ansible will have set it when it loads the plugin.
      # Also note the use of the display object to print to screen. This is available to all callbacks, and you should use this over printing yourself
      self._display.display(
         "munnox_bootstrap.exploration_stdout -> "
         + self._plugin_options['format_string'] % (
            self._days_hours_minutes_seconds(runtime)
          )
      )