
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


# NOTE: Might be in the wrong place but placed here as it is still useful
DOCUMENTATION = r'''
---
module: Bitwarden Create Interface

short_description: Create bitwarden item with CLI to a lookup with credentials.

version_added: '0.1.0'

description:
- A Bitwarden Interface to access tiddlers stored in bitwarden 
- for use in Ansible playbooks to gather information using a filter.

options:
    client_id:
        description: Bitwarden Client ID
        type: string
        required: true
    client_secret:
        description: Bitwarden Client Secret
        type: string
        required: true
    password:
        description: Bitwarden Password
        type: string
        required: true
    records:
        description: Bitwarden records to create
        type: list
        required: true
'''

EXAMPLES = r'''
# Pass in a message
- name: Create record
  munnox.bootstrap.bitwarden.create:
    client_id: "{{ lookup('ansible.builtin.env', 'BW_CLIENTID') }}"
    client_secret: "{{ lookup('ansible.builtin.env', 'BW_CLIENTSECRET') }}"
    password: "{{ lookup('ansible.builtin.env', 'BW_PASSWORD') }}"
    records:
      - name: 'Testing Creation'
        notes: |
            {{ 'My note testing ' + ansible_date_time.iso8601 }}
            Next Line
        
        username: 'a@b.com'
        password: 'password'
        uri: 'https://example.com'
  register: creation_result
    
- name: 'Show creation'
  ansible.builtin.debug:
    msg: |
      {{ creation_result }}
'''

RETURN = r'''
records:
    description: The chosen bitwarden record.
    type: string
'''

import traceback

# from https://github.com/ansible-collections/kubernetes.core/blob/main/plugins/modules/helm_repository.py
YAML_IMP_ERR = None
try:
    import yaml

    HAS_YAML = True
except ImportError:
    YAML_IMP_ERR = traceback.format_exc()
    HAS_YAML = False

from ansible.module_utils._text import to_bytes, to_native, to_text

# from ansible.utils.display import Display
# from ansible.parsing.yaml import Yaml
# from ansible.parsing.yaml.dumper import AnsibleDumper
from ansible.module_utils.basic import AnsibleModule

from ansible_collections.munnox.bootstrap.plugins.module_utils.bitwarden import Bitwarden_CLI

import urllib3

# display = Display()

def run_module():
    """Retrieve Bitwarden records using the server API and a filter."""

    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        client_id=dict(type='str', required=True, no_log=True),
        client_secret=dict(type='str', required=True, no_log=True),
        password=dict(type='str', required=True, no_log=True),
        records=dict(type='list', required=True),
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    client_id = module.params['client_id']
    client_secret = module.params['client_secret']
    password = module.params['password']
    records = module.params['records']

    # display.warning(f"Records: {records}")

    # secure_warning = module.params['secure_warning']
    try:
        session = Bitwarden_CLI.bw_login_api(client_id, client_secret, password=password)
    except Exception as error:
        print(f"Bitwarden session id filter done. Error: {error} ")
        raise error

    record_results = []
    for record in records:
        try:
            record_result = Bitwarden_CLI.bw_create_record(
                session_id=session['session_id'],
                item_name='item',
                item_obj=record
            )
            result['changed'] = True
            record_results.append(record_result)

        except Exception as e:
            result['message'] = f"Bitwarden Get failed type: {type(e)} - {to_native(e)}\n{traceback.format_exc()}",
            result['failed'] = True

    result["records"] = record_results

    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()
