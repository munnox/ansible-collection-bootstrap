
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


# NOTE: Might be in the wrong place but placed here as it is still useful
DOCUMENTATION = r'''
---
module: Bitwarden Get Interface

short_description: Sync bitwarden with CLI to a lookup with credentials.

version_added: '0.1.0'

description:
- A Bitwarden Interface to access tiddlers stored in bitwarden 
- for use in Ansible playbooks to gather information using a filter.

options:
    client_id:
        description: Bitwarden Client ID
        type: string
        required: true
    client_secret:
        description: Bitwarden Client Secret
        type: string
        required: true
    password:
        description: Bitwarden Password
        type: string
        required: true
    record_ids:
        description: Bitwarden record ids
        type: list
        required: true
    search_name:
        description: Bitwarden record search name
        type: string
        required: true
'''

EXAMPLES = r'''
# Pass in a message
- name: Get record
  munnox.bootstrap.bitwarden.get:
    client_id: "{{ lookup('ansible.builtin.env', 'BW_CLIENTID') }}"
    client_secret: "{{ lookup('ansible.builtin.env', 'BW_CLIENTSECRET') }}"
    password: "{{ lookup('ansible.builtin.env', 'BW_PASSWORD') }}"
    record_id: guid
- name: List records
  munnox.bootstrap.bitwarden.get:
    password: "{{ lookup('ansible.builtin.env', 'BW_PASSWORD') }}"
    # record_id: null
- name: Search records
  munnox.bootstrap.bitwarden.get:
    password: "{{ lookup('ansible.builtin.env', 'BW_PASSWORD') }}"
    # record_id: null
    search_name: testing
'''

RETURN = r'''
records:
    description: The chosen bitwarden record.
    type: string
'''

import traceback

# from https://github.com/ansible-collections/kubernetes.core/blob/main/plugins/modules/helm_repository.py
YAML_IMP_ERR = None
try:
    import yaml

    HAS_YAML = True
except ImportError:
    YAML_IMP_ERR = traceback.format_exc()
    HAS_YAML = False

from ansible.module_utils._text import to_bytes, to_native, to_text

# from ansible.utils.display import Display
# from ansible.parsing.yaml import Yaml
# from ansible.parsing.yaml.dumper import AnsibleDumper
from ansible.module_utils.basic import AnsibleModule

from ansible_collections.munnox.bootstrap.plugins.module_utils.bitwarden import Bitwarden_CLI

import urllib3

# display = Display()

def run_module():
    """Retrieve Bitwarden records using the server API and a filter."""

    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        client_id=dict(type='str', required=True, no_log=True),
        client_secret=dict(type='str', required=True, no_log=True),
        password=dict(type='str', required=True, no_log=True),
        record_ids=dict(type='list', required=False),
        search_name=dict(type='str', required=False),
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )


    client_id = module.params['client_id']
    client_secret = module.params['client_secret']
    password = module.params['password']
    record_ids = module.params['record_ids']
    search_name = module.params['search_name']

    result['invocation'] = {
        "module_arg": {
            "client_id": client_id,
            "client_secret": client_secret,
            # "password": password,
            "record_ids": record_ids,
            "search_name": search_name,
        }
    }

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # secure_warning = module.params['secure_warning']
    try:
        session_id = Bitwarden_CLI.bw_login_api(client_id, client_secret, password=password)
    except Exception as error:
        # display.v(f"Bitwarden session id filter done. Error: {error} ")
        raise error
    
    records = []
    try:
        # print("testing")

        list_records = False
        if record_ids is None:
            list_records = True
        
        if list_records:
            try:
                records = Bitwarden_CLI.bw_list_records(
                    session_id=session_id['session_id']
                )
                def record_finder(rec):
                   return rec['name'].find(search_name) >= 0

                if search_name is not None and search_name != "" and search_name.isalnum():

                    result['message'] = f"Searching: '{search_name}'"
                    records = list(filter(record_finder, records))
                    # records = records[:1] # .filter(record_finder)
            except Warning as error:
                print(error)
            except Exception as error:
                raise 
            
        else:
            for record_id in record_ids:
                try:
                    record = Bitwarden_CLI.bw_get_record(
                        session_id=session_id['session_id'],
                        record_id=record_id
                    )
                    records.append(record)
                except Warning as error:
                    print(error)
                except Exception as error:
                    raise error

    except Exception as e:
        result['message'] = f"Bitwarden Get failed type: {type(e)} - {to_native(e)}\n{traceback.format_exc()}",
        result['failed'] = True

    result["records"] = records

    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()
