
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

# NOTE: Might be in the wrong place but placed here as it is still useful
DOCUMENTATION = r'''
---
module: Tiddlywiki Interface

short_description: Search for Tiddlywiki Records with filters using my python API to a lookup with credensials and a URL.

version_added: '0.1.0'

description:
- A Tiddlywiki Interface to access tiddlers stored in a Tiddlywiki collections
- for use in Ansible playbooks to search for information using a filter.

options:
    url:
        description: A tiddlywiki url.
        type: string
        required: true
    filters:
        description: A tiddler filters
        type: list
        required: true
    auth:
        description: A tiddlywiki basic auth.
        type: list
        required: false
    timeout:
        description: Timeout for the connection in seconds
        type: int
        required: false
    cert_verify:
        description: A flag to control weather the serivce cert will be vaidated
        type: bool
        required: false
    secure_warning:
        description: A flag to suppress the urllib certificat verify warning.
        type: bool
        required: false
'''

EXAMPLES = r'''
# Pass in a message
- name: Test with a message
  munnox.bootstrap.tiddlywiki.filter:
    url: http://localhost:8000
    auth:
      - username
      - password
    filters:
      - "[all[tiddlers]!is[system]sort[title]]"
    secure_warning: true
'''

RETURN = r'''
url:
    description: The chosen tiddlywiki url.
    type: string
tiddlers:
    description: The chosen tiddlywiki tiddler.
    type: raw
'''

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils._text import to_native

# Futher info here https://gitlab.com/munnox/python-tiddlywiki
import tiddlywiki

import urllib3

def run_module():
    """Retrieve Tiddlywiki tiddlers using the server API and a filter."""

    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        url=dict(type='str', required=True),
        filters=dict(type='list', required=False, default=['[all[tiddlers]!is[system]sort[title]]']),
        auth=dict(type='list', required=False, default=None),
        timeout=dict(type='int', required=False, default=30),
        cert_verify=dict(type='bool', required=False, default=False),
        secure_warning=dict(type='bool', required=False, default=False)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        url=None,
        # original_message='',
        # message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    tw_url = module.params['url']
    tiddler_filters = module.params['filters']
    auth = module.params['auth']
    timeout_sync = module.params['timeout']
    cert_verify = module.params['cert_verify']
    secure_warning = module.params['secure_warning']

    result["url"] = tw_url
    result['invocation'] = {
        "module_arg": {
            "url": tw_url,
            "auth": auth,
            "filters": tiddler_filters,
            "timeout": timeout_sync,
            "cert_verify": cert_verify,
            "secure_warning": secure_warning
        }
    }

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    if secure_warning:
        urllib3.disable_warnings()

    tiddlers = []
    for tiddler_filter in tiddler_filters:
        try:
            tw_client = tiddlywiki.twclient.TWClient(tw_url, verify=cert_verify, auth=tuple(auth))
            tiddlers_found = tw_client.search(tiddler_filter)
            tiddler_obj = {
                "item": tiddler_filter,
                "tiddler": [ tiddler.to_dict() for tiddler in tiddlers_found ],
                "failed": False,
            }
        except tiddlywiki.errors.TWError as e:
            msg_error = f"tiddlywiki Search failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_filter: {tiddler_filter}"
            result['message'] = "Error encountered see results"
            result['failed'] = True
            tiddler_obj = {
                "item": tiddler_filter,
                "tiddler": None,
                "failed": True,
                "message": msg_error
            }
        except Exception as e:
            msg_error = f"Tiddlywiki Search general failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_filter: {tiddler_filter}"
            result['message'] = "Error encountered see results"
            result['failed'] = True
            tiddler_obj = {
                "item": tiddler_filter,
                "tiddler": None,
                "failed": True,
                "message": msg_error
            }
        tiddlers.append(tiddler_obj)

    result["tiddlers"] = tiddlers

    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()
