

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type



# NOTE: Might be in the wrong place but placed here as it is still useful
DOCUMENTATION = r'''
---
module: Tiddlywiki Interface Set

short_description: Save a Tiddlywiki Record using my python API to a lookup with credensials and a URL.

version_added: '0.1.0'

description:
- A Tiddlywiki Interface to save tiddlers to be stored in a Tiddlywiki collections
- for use in Ansible playbooks to write information.

options:
    url:
        description: A tiddlywiki url.
        type: string
        required: true
    auth:
        description: A tiddlywiki basic auth.
        type: list
        required: false
    tiddlers:
        description: The contents of a list tiddlers to save.
        type: raw
        required: true
    timeout:
        description: Timeout for the connection in seconds
        type: int
        required: false
    cert_verify:
        description: A flag to control weather the serivce cert will be vaidated
        type: bool
        required: false
    secure_warning:
        description: A flag to suppress the urllib certificat verify warning.
        type: bool
        required: false
'''

EXAMPLES = r'''
# Pass in a message
- name: Test with a message
  munnox.bootstrap.tiddlywiki.save:
    url: http://localhost:5000/
    auth:
      - username
      - password
    tiddlers:
      - title: test_{{ now(utc=True, fmt='%Y-%m-%d')  }}
        text: tiddler text
    timeout: 30
'''

RETURN = r'''
url:
    description: The chosen tiddlywiki url.
    type: string
results:
    description: The chosen tiddlywiki tiddler save receipts.
    type: raw
'''

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils._text import to_native

# Futher info here https://gitlab.com/munnox/python-tiddlywiki
import tiddlywiki

import urllib3

def run_module():
    """Save Tiddlywiki tiddlers using the server API."""

    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        url=dict(type='str', required=True),
        auth=dict(type='list', required=False, default=None),
        # id=dict(type='str', required=True),
        tiddlers=dict(type='raw', required=True),
        timeout=dict(type='int', required=False, default=30),
        cert_verify=dict(type='bool', required=False, default=False),
        secure_warning=dict(type='bool', required=False, default=False)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        failed=False,
        changed=False,
        url=None,
        results=[],
        # message='',
        # logs=[]

    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    tw_url = module.params['url']
    auth = module.params['auth']

    tiddlers_content = module.params['tiddlers']

    timeout_sync = module.params['timeout']

    cert_verify = module.params['cert_verify']
    secure_warning = module.params['secure_warning']
    
    result["url"] = tw_url
    result['invocation'] = {
        "module_arg": {
            "url": tw_url,
            "auth": auth,
            "tiddlers": tiddlers_content,
            "timeout": timeout_sync,
            "cert_verify": cert_verify,
            "secure_warning": secure_warning
        }
    }

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)


    if secure_warning:
        urllib3.disable_warnings()

    result_saves = []
    try:
        tw_client = tiddlywiki.twclient.TWClient(tw_url, verify=cert_verify, auth=tuple(auth))
    except Exception as e:
        result["message"].append(f"Error in Tiddlywiki connection: {e}")
        result["failed"] = True

    # result_saves.append(tiddlers_content)
    for tiddler in tiddlers_content:
        try: 
            tid = tiddlywiki.Tiddler(**tiddler)
            save_result = tw_client.save(tid)
            result_obj = {
                "item": tiddler,
                "result": save_result.to_dict(),
                "failed": False,
            }
        except Exception as e:
            msg_error = f"Tiddlywiki Save general failed type: {type(e)} - {to_native(e)} tw_url: {tw_url}, tiddler_title: {tiddler['title']}"
            result['message'] = "Error encountered see results"
            result['failed'] = True
            result_obj = {
                "item": tiddler,
                "tiddler": None,
                "failed": True,
                "message": msg_error
            }
        result_saves.append(result_obj)

    result["results"] = result_saves

    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()
