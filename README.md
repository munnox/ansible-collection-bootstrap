# Ansible Collection - munnox.bootstrap

A Collection to use and bootstrap servers and systems.
Mostly made our of other Repos I have and figure it out and improve it as time allows.

Published publically since it mean I can include it without passwords simplifing initial setup.

The plan is to use this onboard and setup machines for more detailed controls to be put in place.

Basically this is here because:

* It might be useful and 
* Just because you know how the lock works doen't mean people can pick it but they might help design a better lock

## Content

### Roles

Simple open roles to bootstrap services and systems.

| Role Name | Description |
| --------- | ----------- |
| `common` | Utility Task file for various common helpers tasks list e.g. Run a Ping summary, turn swap off, add NFS mount, nfs server, passwordless sudo fixer etc. [Link to Readme](roles/common/README.md) |
| `libvirt` | Create and remove VM guests from a (Libvirt) KVM Hypervisor [Link to Readme](roles/libvirt/README.md) |
| `aws` | Enforce state and type of EC2 VM guests from a (Libvirt) KVM Hypervisor [Link to Readme](roles/aws/README.md) |
| `k3s` | Installs k3s Kubernetes deployment service from Rancher [Link to Readme](roles/k3s/README.md) |
| `microk8s` | Installs Microk8s snap service from Canonical [Link to Readme](roles/microk8s/README.md) |
| `docker` | Install docker service on a host [Link to Readme](roles/docker/README.md) |
| `gitea` | Install gitea service on a host [Link to Readme](roles/gitea/README.md) |
| `podman` | Install podman service on a host [Link to Readme](roles/podman/README.md) |
| `postgresql` | Install Postgresql and PGadmin on a host machine need human interaction for final setup [Link to Readme](roles/posgresql/README.md) |
| `ceph` | Install Ceph on a host machine need human interaction for final setup [Link to Readme](roles/ceph/README.md) |
| `unifi` | Installs Unifi server on a host machine [Link to Readme](roles/unifi/README.md) |
| `awx` | Add AWX operator from git repo to a kubernetes machines[Link to Readme](roles/awx/README.md) |
| `nix` | Trys to install Nix by using the install scripts but needs human interaction for final setup[Link to Readme](roles/nix/README.md) |
| `elk` | Trys to install ELK elasticsearch Logstash and Kibnana by using the install scripts **WIP** [Link to Readme](roles/elk/README.md) |
| `devstack` | Install Devstack [Link to Readme](roles/devstack/README.md) |


### Playbooks

There are two main type of playbook:

* `debugging` - Example and useful playbook to aid debugging information, hosts and plays.
* `examples` - example playbook to try something or to make it easy to test something.
* `init` - Playbook useful to initialise and bootstrap.
* `services` - Opinionated playbook that run and act as exmaple documenation to the roles included.

To use add the following to a playbook.

```yaml
- name: Import the test playbook from munnox.bootstrap
  ansible.builtin.import_playbook: munnox.bootstrap.examples.test_playbook.yml
```

#### Examples

`testing_collection_examples.yml` - A try out playbook that run local and tryies out the Plugins included needs `BW_PASSWORD` and `TW_URL` to use the Bitwarden filter and Tiddlywiki filter respectively.

## Requirements

Need the following Collections:

* `community.libvirt`
* `community.docker`

## Install

### `requirements.yml`

```yaml
- name: git@gitlab.com:munnox/ansible-collection-bootstrap.git
  type: git
  version: main
```

```bash
# install based on collections specified
ansible-galaxy collection install -r requirements.yml
# Upgrade collections
ansible-galaxy collection install --upgrade -r requirements.yml
# Upgrade and force the repull of collections
ansible-galaxy collection install --upgrade -r requirements.yml --force
```

# Development

## Local

```bash
poetry install --with dev
```

## Create an EE:

### For any platform:

```bash
poetry run ansible-builder create --output-filename Dockerfile
docker build --platform linux/amd64 -f context/Dockerfile context -t dev_bootstrap_ee:latest
docker run -it --rm -v $(pwd):/runner --platform linux/amd64 dev_bootstrap_ee:latest       
```

### As simple build:

```bash
poetry run ansible-builder build --container-runtime docker -t dev_bootstrap_ee:latest
```

## Use the EE:

```bash
docker run -it --rm -v $(pwd):/runner dev_bootstrap_ee:latest 
```

## To prep

```bash
poetry run black .
```

## To test

```bash
source .env
poetry run molecule test
```

# License

GPLv3 Or Creative Commons Attributions

Basically some of this might be useful. Do not blame me just don't use it :)

# Author Information

By Dr Robert Munnoch PhD Meng MIET
